// Fill out your copyright notice in the Description page of Project Settings.


#include "Bullet.h"

#include "Engine/EngineTypes.h"
#include "Kismet/KismetMathLibrary.h"
#include "Particles/ParticleSystemComponent.h"

// Sets default values

ABullet::ABullet()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	CollisionSphere->SetRelativeLocation(FVector::ZeroVector);
	
	ParticleSystemComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleSystemComponent"));
	ParticleSystemComponent->AttachToComponent(CollisionSphere, FAttachmentTransformRules::SnapToTargetIncludingScale);

	EnergyBulletComponent = CreateDefaultSubobject<UEnergyBulletComponent>(TEXT("EnergyBulletComponent"));

	static ConstructorHelpers::FObjectFinder<UParticleSystem> DefParticleObj(
		TEXT("ParticleSystem'/Game/FXVarietyPack/Particles/P_ky_waterBall1.P_ky_waterBall1'"));
	DefParticle = DefParticleObj.Object;
	//	ParticleSystemComponent->Template = DefParticle;
	static ConstructorHelpers::FObjectFinder<UParticleSystem> CollisionParticleObj(
		TEXT("ParticleSystem'/Game/FXVarietyPack/Particles/P_ky_waterBallHit1.P_ky_waterBallHit1'"));
	CollisionParticle = CollisionParticleObj.Object;
	OnActorHit.AddDynamic(this, &ABullet::OnHit);
}

void ABullet::Setup(float MinSize, float MaxSize)
{
	ResetGraphics();
	
	CollisionSphere->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	// CollisionSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	// CollisionSphere->SetCollisionObjectType(ECC_Visibility);
	// CollisionSphere->SetCollisionResponseToChannel(Laser_channel, ECollisionResponse::ECR_Ignore);
	CollisionSphere->SetNotifyRigidBodyCollision(true);
	CollisionSphere->SetSimulatePhysics(true);

}

void ABullet::ResetGraphics()
{
	
	ParticleSystemComponent->SetTemplate(DefParticle);
}

// Called when the game starts or when spawned
void ABullet::BeginPlay()
{
	Super::BeginPlay();
}

void ABullet::OnHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	CollisionSphere->ComponentVelocity = FVector::ZeroVector;
	CollisionSphere->SetNotifyRigidBodyCollision(false);
	CollisionSphere->SetSimulatePhysics(false);
	CollisionSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	ParticleSystemComponent->SetTemplate(CollisionParticle);
	AEnemy_Level2* target = Cast<AEnemy_Level2>(OtherActor);
	if(IsValid(target))
	{
		target->Die();
	}
}

// Called every frame
void ABullet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABullet::Grow(const float Alpha)
{
	float Scale = UKismetMathLibrary::Lerp(Min_scale, Max_scale, Alpha);
	
	SetActorScale3D(FVector(Scale, Scale, Scale));
}
