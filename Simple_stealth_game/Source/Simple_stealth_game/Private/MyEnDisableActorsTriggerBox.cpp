// Fill out your copyright notice in the Description page of Project Settings.


#include "MyEnDisableActorsTriggerBox.h"


#include "Particles/ParticleSystemComponent.h"
#include "Simple_stealth_game/FP_FirstPerson/FP_FirstPersonCharacter.h"

void AMyEnDisableActorsTriggerBox::NotifyActorBeginOverlap(AActor* OtherActor)
{
	AFP_FirstPersonCharacter* Character = Cast<AFP_FirstPersonCharacter>(OtherActor);
	if (Character != nullptr)
	{
		for (int i = 0; i < DisActors.Num(); i++)
		{
			DisActors[i]->SetActorHiddenInGame(true);
		}

		for (int i = 0; i < EnActors.Num(); i++)
		{
			EnActors[i]->SetActorHiddenInGame(false);
			UParticleSystemComponent* Component = Cast< UParticleSystemComponent> (EnActors[i]->GetComponentByClass(UParticleSystemComponent::StaticClass()));

			if (Component) { Component->Activate(true); }
		}

		SetActorEnableCollision(false);
	}
}