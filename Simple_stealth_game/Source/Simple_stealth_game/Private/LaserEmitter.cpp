// Fill out your copyright notice in the Description page of Project Settings.


#include "LaserEmitter.h"

#include "Simple_stealth_game/Public/Laser_beam.h"

// Sets default values
ALaserEmitter::ALaserEmitter()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ALaserEmitter::BeginPlay()
{
	Super::BeginPlay();
	FTimerHandle UnusedHandle;
	FActorSpawnParameters Parameters;
	//beams.Add(GetWorld()->SpawnActor<ALaser_beam>(GetActorLocation(), GetActorRotation()));
	beams.Add(GetWorld()->SpawnActor<ALaser_beam>(laserbeam_Pref, GetActorLocation(), GetActorRotation(), Parameters));
	beams[0]->SetActorTickEnabled(false);
	beams[0]->SetActorHiddenInGame(true);
	beams[0]->SetActorEnableCollision(false);


	GetWorldTimerManager().SetTimer(UnusedHandle, this, &ALaserEmitter::Cast_light, .1f, true, 0.1f);
}

void ALaserEmitter::Tick(float DeltaSeconds)
{
//	Cast_light();
}

void ALaserEmitter::Cast_light()
{
	FVector cast_origin = GetActorLocation();
	FVector cast_direction = GetActorForwardVector();
	current_beam_index = 0;
	cast_continue = true;
	for (ALaser_beam* beam : beams)
	{
		beam->SetActorTickEnabled(false);
		beam->SetActorHiddenInGame(true);
		beam->SetActorEnableCollision(false);
	}
	bool energy_beam = false;
	while (cast_continue)
	{
		bool needReloadBeams = false;
		beam_start = cast_origin;
		FHitResult HitResult;
		if (GetWorld()->LineTraceSingleByChannel(HitResult, cast_origin, cast_origin + (cast_direction * cast_distance),
		                                         cast_channel))
		{
			beam_end = cast_origin = HitResult.ImpactPoint;
			int SectionIndex;

			UMaterialInterface* Hit_material = HitResult.Component->GetMaterialFromCollisionFaceIndex(0, SectionIndex);

			if (Hit_material == mirror_mat)
			{
				cast_direction = cast_direction.MirrorByVector(HitResult.ImpactNormal);
				cast_continue = true;
			}
			else
			{
				if (Hit_material == sensor_mat)
				{
					if(!energy_beam) needReloadBeams = true;
					energy_beam = true;
					cast_continue = false;
				}
				else
				{
					cast_continue = false;
				}
			}
		}
		else
		{
		
			beam_end = cast_origin + (cast_direction * cast_distance);
			cast_continue = false;
		}

		if (current_beam_index == beams.Num())
		{
			FActorSpawnParameters Parameters;
			Parameters.Template = Cast<ALaser_beam>(laserbeam_Pref);

			beams.Add(GetWorld()->SpawnActor<ALaser_beam>(laserbeam_Pref, GetActorLocation(), GetActorRotation(),
			                                              Parameters));
		}
if(needReloadBeams)
{
	for(int i = 0; i <current_beam_index; i++)
	{
		beams[i]->SetMaterial(energy_beam);
	}
}
		beams[current_beam_index]->Set_end(beam_start, beam_end);
		beams[current_beam_index]->SetActorTickEnabled(true);
		beams[current_beam_index]->SetActorHiddenInGame(false);
		beams[current_beam_index]->SetActorEnableCollision(true);
		beams[current_beam_index]->SetMaterial(energy_beam);
		current_beam_index++;
		
	}
}
