// Fill out your copyright notice in the Description page of Project Settings.


#include "LaserActorBase.h"

// Sets default values
ALaserActorBase::ALaserActorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ALaserActorBase::BeginPlay()
{
	Super::BeginPlay();
	InitPos = GetActorLocation();
	InitRot = GetActorRotation();
}

// Called every frame
void ALaserActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALaserActorBase::Reset()
{
	SetActorLocation(InitPos);
	SetActorRotation(InitRot);
}

