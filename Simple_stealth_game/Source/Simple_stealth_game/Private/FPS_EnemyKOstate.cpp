// Fill out your copyright notice in the Description page of Project Settings.


#include "FPS_EnemyKOstate.h"

#include "Kismet/KismetMathLibrary.h"

void UFPS_EnemyKOstate::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration)
{
	duration_total = TotalDuration;
	duration_remaining = TotalDuration;
	enemy_owner = Cast<AEnemy_Level2>(MeshComp->GetOwner());
	MeshComp->SetCollisionProfileName("ragdoll");
	MeshComp->SetAllBodiesBelowSimulatePhysics("pelvis", true, true);
	endColor = UKismetMathLibrary::LinearColorLerp( FLinearColor::Gray, FLinearColor::Black, 0.8f);
}

void UFPS_EnemyKOstate::NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime)
{
	duration_remaining-= FrameDeltaTime;
	if(IsValid(enemy_owner))
	{
		float alpha = 1 - (duration_remaining / duration_total);
		FLinearColor color = UKismetMathLibrary::LinearColorLerp(FLinearColor::Red, endColor, alpha);
		enemy_owner->UpdateBodyColor(color);
	}
}
