// Fill out your copyright notice in the Description page of Project Settings.


#include "BorderTriggerBox.h"
#include "Simple_stealth_game/FP_FirstPerson/FP_FirstPersonCharacter.h"

void ABorderTriggerBox::NotifyActorBeginOverlap(AActor* OtherActor)
{
	AFP_FirstPersonCharacter* Character = Cast<AFP_FirstPersonCharacter>(OtherActor);
	if(Character != nullptr)
	{
		Character->ResetGrabbedLaser();
	}
}
