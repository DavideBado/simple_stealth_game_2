// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy_Level2.h"

#include "Perception/PawnSensingComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Simple_stealth_game/FP_FirstPerson/FP_FirstPersonCharacter.h"

// Sets default values
AEnemy_Level2::AEnemy_Level2()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PawnSensingComp = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComp"));
	PawnSensingComp->OnSeePawn.AddDynamic(this, &AEnemy_Level2::OnPawnSeen);

	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	FP_Gun->SetupAttachment(GetMesh(), TEXT("hand_r"));
}

// Called when the game starts or when spawned
void AEnemy_Level2::BeginPlay()
{
	Super::BeginPlay();
	BodyMaterial = GetMesh()->CreateDynamicMaterialInstance(0, GetMesh()->GetMaterial(0));
}

void AEnemy_Level2::OnPawnSeen(APawn* SeenPawn)
{
	if (SeenPawn == nullptr)
	{
		CanPlaySound = false;
		UE_LOG(LogTemp, Warning, TEXT("#################"))
		return;
	}
	player = Cast<AFP_FirstPersonCharacter>(SeenPawn);
	if (IsValid(player))
	{
		if (FVector::Distance(GetActorLocation(), player->GetActorLocation()) <= playerShootDist)
		{
			Shoot(player->GetActorLocation());
		}
		else if (AlertSounds[0] != NULL && CanPlaySound)
		{
			CanPlaySound = false;
			AlertSoundsTimer = FApp::GetCurrentTime();
			UGameplayStatics::PlaySoundAtLocation(this, AlertSounds[FMath::RandRange(0, AlertSounds.Num() - 1)],
			                                      GetActorLocation());
		}
		else
		{
			if(FApp::GetCurrentTime() - AlertSoundsTimer >= 2.f) CanPlaySound = true;
		}
	}
}

void AEnemy_Level2::Shoot(FVector target)
{
	if (!Alive) return;
	if (player->Alive)
		player->Die();


	GetMesh()->GetAnimInstance()->Montage_Play(ShootAnim);
	FVector cast_origin = FP_Gun->GetComponentLocation();
	FHitResult HitResult;
	FCollisionShape CollisionShape = FCollisionShape::MakeSphere(20.f);
	if (GetWorld()->SweepSingleByChannel(HitResult, cast_origin, cast_origin + (FP_Gun->GetRightVector() * 100000),
	                                     FQuat::Identity, cast_channel, CollisionShape))
	{
		AFP_FirstPersonCharacter* Hit_Player = Cast<AFP_FirstPersonCharacter>(HitResult.GetActor());
		if (IsValid(Hit_Player))
			Hit_Player->Die();
	}
}

// Called every frame
void AEnemy_Level2::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AEnemy_Level2::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AEnemy_Level2::Die()
{
	Alive = false;
	GetCharacterMovement()->DisableMovement();
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn,
	                                                     ECollisionResponse::ECR_Overlap);
	GetMesh()->GetAnimInstance()->Montage_Play(DeathAnim);
	BodyMaterial->SetVectorParameterValue("BodyColor", FLinearColor::Red);
	PawnSensingComp->Deactivate();
}

void AEnemy_Level2::UpdateBodyColor(FLinearColor color)
{
	BodyMaterial->SetVectorParameterValue("BodyColor", color);
}
