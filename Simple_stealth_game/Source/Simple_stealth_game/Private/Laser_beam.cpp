// Fill out your copyright notice in the Description page of Project Settings.


#include "Laser_beam.h"

#include "Kismet/KismetMathLibrary.h"

// Sets default values
ALaser_beam::ALaser_beam()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ALaser_beam::BeginPlay()
{
	Super::BeginPlay();
	UStaticMeshComponent* mesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	Material = mesh->CreateDynamicMaterialInstance(0, mesh->GetMaterial(0));
}

// Called every frame
void ALaser_beam::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ALaser_beam::Set_end(FVector Begin, FVector End)
{
	SetActorLocationAndRotation(Begin, UKismetMathLibrary::MakeRotFromZ((End - Begin).GetSafeNormal()));
	SetActorScale3D(FVector(0.05f, 0.05f, (End - Begin).Size() / 100.f));
}

void ALaser_beam::SetMaterial(bool UseEnergyMat)
{
	Energy_beam = UseEnergyMat;
	FLinearColor color;
	if (UseEnergyMat)color =FLinearColor::Red;
	else color = FLinearColor::Blue;
	Material->SetVectorParameterValue("Color", color);
}
