// Fill out your copyright notice in the Description page of Project Settings.


#include "EnergyBulletComponent.h"

#include "Kismet/KismetMathLibrary.h"
#include "Particles/ParticleSystemComponent.h"

// Sets default values for this component's properties
UEnergyBulletComponent::UEnergyBulletComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UEnergyBulletComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UEnergyBulletComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UEnergyBulletComponent::Kill_enemy(AActor* enemy, UParticleSystemComponent* particleComponent, UParticleSystem* particle)
{
	particleComponent->Template = particle;
	AEnemy_Level2* target = Cast<AEnemy_Level2>(enemy);
	if(IsValid((target)))
	{
		target->Die();
	}
}

void UEnergyBulletComponent::Grow(const float Alpha)
{
	const float Scale = UKismetMathLibrary::Lerp(min_scale, max_scale, Alpha);
	GetOwner()->SetActorScale3D(FVector(Scale, Scale, Scale));
}


