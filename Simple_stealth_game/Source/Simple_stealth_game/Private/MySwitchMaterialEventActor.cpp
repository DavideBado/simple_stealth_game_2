// Fill out your copyright notice in the Description page of Project Settings.


#include "MySwitchMaterialEventActor.h"

void AMySwitchMaterialEventActor::Event()
{
	Super::Event();
	for (int i = 0; i < TargetActors.Num(); i++)
	{
		UMeshComponent* MeshComp = Cast<UMeshComponent>(TargetActors[i]->GetComponentByClass(UMeshComponent::StaticClass()));
		if (IsValid(MeshComp))
		MeshComp->SetMaterial(MeshIndex, Switch_Mat);
	}
}
