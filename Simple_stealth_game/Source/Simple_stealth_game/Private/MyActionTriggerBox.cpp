// Fill out your copyright notice in the Description page of Project Settings.


#include "MyActionTriggerBox.h"

#include "Particles/ParticleSystemComponent.h"
#include "Simple_stealth_game/FP_FirstPerson/FP_FirstPersonCharacter.h"

void AMyActionTriggerBox::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);
	AFP_FirstPersonCharacter* Character = Cast<AFP_FirstPersonCharacter>(OtherActor);
	if (Character != nullptr)
	{
		EnableInput(Cast<APlayerController>(Character->GetController()));		
	}
}

void AMyActionTriggerBox::OnInput()
{
	for (int i = 0; i < EventActors.Num(); i++)
	{
		EventActors[i]->Event();
	}

	for (int i = 0; i < OnInput_DisActors.Num(); i++)
	{
		OnInput_DisActors[i]->SetActorHiddenInGame(true);
	}

	for (int i = 0; i < OnInput_EnActors.Num(); i++)
	{
		OnInput_EnActors[i]->SetActorHiddenInGame(false);
		UParticleSystemComponent* Component = Cast< UParticleSystemComponent> (OnInput_EnActors[i]->GetComponentByClass(UParticleSystemComponent::StaticClass()));

		if (Component) { Component->Activate(true); }
	}

	for (int i = 0; i < OnInputTriggers.Num(); i++)
	{
		OnInputTriggers[i]->GetCollisionComponent()->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
}

void AMyEnDisableActorsTriggerBox::NotifyActorEndOverlap(AActor* OtherActor)
{
	AFP_FirstPersonCharacter* Character = Cast<AFP_FirstPersonCharacter>(OtherActor);
	if (Character != nullptr)
	{
		DisableInput(Cast<APlayerController>(Character->GetController()));
	}
}
