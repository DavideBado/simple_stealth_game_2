// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Engine/TriggerBox.h"
#include "MyEnDisableActorsTriggerBox.generated.h"

/**
 * 
 */
UCLASS()
class SIMPLE_STEALTH_GAME_API AMyEnDisableActorsTriggerBox : public ATriggerBox
{
	GENERATED_BODY()

protected:
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	virtual void NotifyActorEndOverlap(AActor* OtherActor) override;

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<AActor*> DisActors;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<AActor*> EnActors;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<AActor*> OnInput_EnActors;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<AActor*> OnInput_DisActors;
};
