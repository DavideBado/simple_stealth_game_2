// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Laser_beam.generated.h"

UCLASS()
class SIMPLE_STEALTH_GAME_API ALaser_beam : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ALaser_beam();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void Set_end(FVector Begin, FVector End);
	void SetMaterial(bool UseEnergyMat);
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* static_mesh;
	UPROPERTY()
	UMaterialInstanceDynamic* Material;
	bool Energy_beam = false;
};
