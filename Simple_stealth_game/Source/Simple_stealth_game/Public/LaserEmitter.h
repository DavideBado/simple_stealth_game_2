// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Laser_beam.h"
#include "LaserActorBase.h"
#include "LaserEmitter.generated.h"

UCLASS()
class SIMPLE_STEALTH_GAME_API ALaserEmitter : public ALaserActorBase
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALaserEmitter();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<ECollisionChannel> cast_channel;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMeshComponent* emitterComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ALaser_beam> laserbeam_Pref;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterial* mirror_mat;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterial* sensor_mat;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float cast_distance = 1000.f;

protected:

	bool cast_continue = true;
	UPROPERTY()
	TArray<ALaser_beam*> beams;
	int current_beam_index;
	FVector beam_start;
	FVector beam_end;
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
void Cast_light();
};
