// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Enemy_Level2.h"
#include "Animation/AnimNotifies/AnimNotifyState.h"
#include "FPS_EnemyKOstate.generated.h"

/**
 * 
 */
UCLASS()
class SIMPLE_STEALTH_GAME_API UFPS_EnemyKOstate : public UAnimNotifyState
{
	GENERATED_BODY()

	virtual void NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration) override;
	virtual void NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime) override;

	UPROPERTY()
	AEnemy_Level2* enemy_owner;
	
	float duration_total = 0.f;
	float duration_remaining = 0.f;
	FLinearColor endColor;
};
