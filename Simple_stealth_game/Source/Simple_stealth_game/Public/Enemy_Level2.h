// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Enemy_Level2.generated.h"


class UPawnSensingComponent;
class AFP_FirstPersonCharacter;
UCLASS()
class SIMPLE_STEALTH_GAME_API AEnemy_Level2 : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy_Level2();
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UAnimMontage* DeathAnim;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UAnimMontage* ShootAnim;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	TArray<USoundBase*> AlertSounds;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<ECollisionChannel> cast_channel;
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* FP_Gun;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UFUNCTION()
	void OnPawnSeen(APawn* SeenPawn);
	void Shoot(FVector target);
	UPROPERTY()
	UMaterialInstanceDynamic* BodyMaterial;
	UPROPERTY(VisibleAnywhere, Category= "Components")
	UPawnSensingComponent* PawnSensingComp;
	UPROPERTY()
	AFP_FirstPersonCharacter* player;
	bool Alive = true;
	float AlertSoundsTimer = 0.f;
	bool CanPlaySound = true;
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	void Die();
	void UpdateBodyColor(FLinearColor color);
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float playerShootDist = 1000.f;
};
