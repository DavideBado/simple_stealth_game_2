// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyEventActor.h"
#include "MyEnDisMatEmissiveEventActor.generated.h"

/**
 * 
 */
UCLASS()
class SIMPLE_STEALTH_GAME_API AMyEnDisMatEmissiveEventActor : public AMyEventActor
{
	GENERATED_BODY()
protected:
	virtual void Event() override;
public:
	UPROPERTY(EditAnywhere)
	bool SetEmissEnable = true;
	UPROPERTY(EditAnywhere)
	int MeshIndex = 0;
	UPROPERTY(EditAnywhere)
	TArray<AActor*> TargetActors;
};
