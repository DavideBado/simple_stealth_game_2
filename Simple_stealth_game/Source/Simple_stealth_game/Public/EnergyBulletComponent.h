// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Enemy_Level2.h"
#include "Components/ActorComponent.h"
#include "EnergyBulletComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SIMPLE_STEALTH_GAME_API UEnergyBulletComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UEnergyBulletComponent();
	UPROPERTY(BlueprintReadWrite)
	float max_scale;
	UPROPERTY(BlueprintReadWrite)
	float min_scale;
	
protected:

	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
UFUNCTION(BlueprintCallable)
	void Kill_enemy(AActor* enemy, UParticleSystemComponent* particleComponent, UParticleSystem* particle);
	void Grow(const float Alpha);
	
};
