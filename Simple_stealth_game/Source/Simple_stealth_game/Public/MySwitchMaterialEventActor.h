// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyEventActor.h"
#include "MySwitchMaterialEventActor.generated.h"

/**
 * 
 */
UCLASS()
class SIMPLE_STEALTH_GAME_API AMySwitchMaterialEventActor : public AMyEventActor
{
	GENERATED_BODY()
	protected:
		virtual void Event() override;
	public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<AActor*> TargetActors;
	UPROPERTY(EditAnywhere)
	int MeshIndex = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterialInstance* Switch_Mat;
};
