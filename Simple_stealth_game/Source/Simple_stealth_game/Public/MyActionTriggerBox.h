// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyEnDisableActorsTriggerBox.h"
#include "MyEventActor.h"

#include "MyActionTriggerBox.generated.h"

/**
 * 
 */
UCLASS()
class SIMPLE_STEALTH_GAME_API AMyActionTriggerBox : public AMyEnDisableActorsTriggerBox
{
	GENERATED_BODY()
	protected:
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	UFUNCTION(BlueprintCallable)
	void OnInput();
	public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<AMyEventActor*> EventActors;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<ATriggerBox*> OnInputTriggers;
};
