// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "BorderTriggerBox.generated.h"

/**
 * 
 */
UCLASS()
class SIMPLE_STEALTH_GAME_API ABorderTriggerBox : public ATriggerBox
{
	GENERATED_BODY()

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	
};
