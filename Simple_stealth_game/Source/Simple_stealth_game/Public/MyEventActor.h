// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyEventActor.generated.h"

UCLASS()
class SIMPLE_STEALTH_GAME_API AMyEventActor : public AActor
{
	GENERATED_BODY()
	friend class AMyActionTriggerBox;
public:	
	// Sets default values for this actor's properties
	AMyEventActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Event() ;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
