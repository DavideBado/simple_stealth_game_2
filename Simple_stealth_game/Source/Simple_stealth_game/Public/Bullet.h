// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "EnergyBulletComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "Bullet.generated.h"

UCLASS()
class SIMPLE_STEALTH_GAME_API ABullet : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABullet();
	void Setup(float MinSize, float MaxSize);
	void ResetGraphics();
	void Grow(float Alpha);
	UPROPERTY(VisibleAnywhere)
	USphereComponent* CollisionSphere;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<ECollisionChannel> Laser_channel;
	UPROPERTY(VisibleAnywhere, Instanced)
	UEnergyBulletComponent* EnergyBulletComponent;
	float Min_scale;
	float Max_scale;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(VisibleAnywhere, Instanced)
	UParticleSystemComponent* ParticleSystemComponent;
	UPROPERTY()
	UParticleSystem* DefParticle;
	UPROPERTY()
	UParticleSystem* CollisionParticle;
	UFUNCTION()
	void OnHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
