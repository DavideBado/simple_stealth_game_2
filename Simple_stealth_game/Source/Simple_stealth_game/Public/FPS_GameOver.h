// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "FPS_GameOver.generated.h"

/**
 * 
 */
UCLASS()
class SIMPLE_STEALTH_GAME_API UFPS_GameOver : public UUserWidget
{
	GENERATED_BODY()
	
};
