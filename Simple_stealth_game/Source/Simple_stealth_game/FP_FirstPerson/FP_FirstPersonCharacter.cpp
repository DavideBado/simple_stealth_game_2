// Copyright Epic Games, Inc. All Rights Reserved.

#include "FP_FirstPersonCharacter.h"

#include  "GameFramework/Actor.h"


#include "FPS_GameOver.h"
#include "LaserActorBase.h"
#include "Laser_beam.h"
#include "Animation/AnimInstance.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Camera/CameraComponent.h"
#include "Engine/EngineTypes.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Particles/ParticleSystemComponent.h"

#define COLLISION_WEAPON		ECC_GameTraceChannel1

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AFP_FirstPersonCharacter

AFP_FirstPersonCharacter::AFP_FirstPersonCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(0, 0, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true); // Set so only owner can see mesh
	Mesh1P->SetupAttachment(FirstPersonCameraComponent); // Attach mesh to FirstPersonCameraComponent
	Mesh1P->bCastDynamicShadow = false; // Disallow mesh to cast dynamic shadows
	Mesh1P->CastShadow = false; // Disallow mesh to cast other shadows

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(true); // Only the owning player will see this mesh
	FP_Gun->bCastDynamicShadow = false; // Disallow mesh to cast dynamic shadows
	FP_Gun->CastShadow = false; // Disallow mesh to cast other shadows
	FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));

	GrabbedObjectLocation = CreateDefaultSubobject<USceneComponent>(TEXT("GrabbedObjectLocation"));
	GrabbedObjectLocation->AttachToComponent(FP_Gun, FAttachmentTransformRules::SnapToTargetIncludingScale);


	static ConstructorHelpers::FObjectFinder<UParticleSystem> FullEnergyParticleObj(
		TEXT("ParticleSystem'/Game/StarterContent/Particles/P_Smoke.P_Smoke'"));
	FullEnergyParticle = FullEnergyParticleObj.Object;

	ParticleSystemComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleSystemComponent"));
	ParticleSystemComponent->AttachToComponent(FP_Gun, FAttachmentTransformRules::SnapToTargetIncludingScale);
	ParticleSystemComponent->SetTemplate(FullEnergyParticle);

	BulletLocation = CreateDefaultSubobject<USceneComponent>(TEXT("BulletLocation"));
	BulletLocation->SetupAttachment(FP_Gun);
}


//////////////////////////////////////////////////////////////////////////
// Input

void AFP_FirstPersonCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Set up gameplay key bindings

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind fire event
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AFP_FirstPersonCharacter::OnFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &AFP_FirstPersonCharacter::LowFire);

	PlayerInputComponent->BindAction("Fire2", IE_Pressed, this, &AFP_FirstPersonCharacter::LoadBullet);

	PlayerInputComponent->BindAction("Fire2", IE_Released, this, &AFP_FirstPersonCharacter::ShootBullet);
	PlayerInputComponent->BindAction("Select", IE_Released, this, &AFP_FirstPersonCharacter::Select);
	PlayerInputComponent->BindAction("Switch", IE_Pressed, this, &AFP_FirstPersonCharacter::Switch);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &AFP_FirstPersonCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AFP_FirstPersonCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AFP_FirstPersonCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AFP_FirstPersonCharacter::LookUpAtRate);
}

void AFP_FirstPersonCharacter::Tick(float DeltaSeconds)
{
	if (Fire2_IsPressed)
	{
		FVector cast_origin = FP_Gun->GetComponentLocation();
		FHitResult HitResult;
		FCollisionShape CollisionShape = FCollisionShape::MakeSphere(30.f);
		if (GetWorld()->SweepSingleByChannel(HitResult, cast_origin, cast_origin + (FP_Gun->GetRightVector() * 150),
		                                     FQuat::Identity, cast_channel, CollisionShape))
		{
			ALaser_beam* Laser_Beam = Cast<ALaser_beam>(HitResult.GetActor());
			if (IsValid(Laser_Beam) && Laser_Beam->Energy_beam)
				ChargeBullet(1);
		}
		else if (Bullets_pool[current_bullet]->GetActorScale().X > 0 && Bullets_pool[current_bullet]->GetActorScale().X
			< 1)
			ChargeBullet(-1);
	}

	else if (Bullets_pool[current_bullet]->GetActorScale().X > 0)ChargeBullet(-1);
}

void AFP_FirstPersonCharacter::Die()
{
	Alive = false;
	LoadGameOverUI.Broadcast();
}

void AFP_FirstPersonCharacter::ResetGrabbedLaser()
{
	if (GrabbedLaser != nullptr)
	{
		GrabbedLaser->Reset();
	}
	Fire2_IsPressed = false;
}

void AFP_FirstPersonCharacter::BeginPlay()
{
	Super::BeginPlay();

	ParticleSystemComponent->Deactivate();

	for (int i = 0; i < Pool_size; i++)
	{
		FActorSpawnParameters params;
		Bullets_pool.Add(GetWorld()->SpawnActor<ABullet>(EnergyBullet));
		Bullets_pool[i]->Laser_channel = Laser_Coll_channel;
		Bullets_pool[i]->SetActorTickEnabled(false);
		Bullets_pool[i]->SetActorHiddenInGame(true);
		Bullets_pool[i]->SetActorEnableCollision(false);
		Bullets_pool[i]->Min_scale = MinSize;
		Bullets_pool[i]->Max_scale = MaxSize;
	}
}

void AFP_FirstPersonCharacter::OnFire()
{
	const FCollisionQueryParams QueryParams(SCENE_QUERY_STAT(GravityGunTrace), false, this);
	const float TraceRange = 2000000.f;
	const FVector StartTrace = FP_Gun->GetComponentLocation();
	const FVector EndTrace = (FP_Gun->GetRightVector() * TraceRange) + StartTrace;
	FHitResult Hit;

	if (GetWorld()->LineTraceSingleByChannel(Hit, StartTrace, EndTrace, ECC_Visibility, QueryParams))
	{
		if (UPrimitiveComponent* Prim = Hit.GetComponent())
		{
			if (Prim->IsSimulatingPhysics())
			{
				SetGrabbedObject(Prim, Hit.GetActor());
			}
		}
	}
}

void AFP_FirstPersonCharacter::EndFire(float Strength = 100.f)
{
	if (GrabbedObject)
	{
		const float ShootStrength = Strength;
		const FVector ShootVelocity = FirstPersonCameraComponent->GetForwardVector() * ShootStrength;
		GrabbedObject->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		GrabbedObject->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
		GrabbedObject->SetSimulatePhysics(true);
		GrabbedObject->AddImpulse(ShootVelocity, NAME_None, true);

		SetGrabbedObject(nullptr, nullptr);


		// Play a sound if there is one
		if (FireSound != NULL)
		{
			UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
		}

		// Try and play a firing animation if specified
		if (FireAnimation != NULL)
		{
			// Get the animation object for the arms mesh
			UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
			if (AnimInstance != NULL)
			{
				AnimInstance->Montage_Play(FireAnimation, 1.f);
			}
		}
	}
}

void AFP_FirstPersonCharacter::LowFire()
{
	EndFire(100.f);
}

void AFP_FirstPersonCharacter::ShootBullet()
{
	Fire2_IsPressed = false;
	if (Bullet_Charged)
	{
		Bullet_Charged = false;
		Bullets_pool[current_bullet]->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		Bullets_pool[current_bullet]->Setup(MinSize, MaxSize);
		Bullets_pool[current_bullet]->CollisionSphere->AddImpulse(FP_Gun->GetRightVector() * 100000.f);
		current_bullet = (current_bullet + 1) % Pool_size;
		Bullets_pool[current_bullet]->SetActorRelativeScale3D(FVector::ZeroVector);
		ParticleSystemComponent->Deactivate();
	}
}

void AFP_FirstPersonCharacter::Select()
{
}

void AFP_FirstPersonCharacter::Switch()
{
	FHitResult HitResult;
	if (GetWorld()->LineTraceSingleByChannel(HitResult, (FP_Gun->GetComponentLocation() + (FP_Gun->GetForwardVector() * 46.f)),
	                                         (FP_Gun->GetComponentLocation() + (FP_Gun->GetForwardVector() * 26.f))+ (
		                                         FP_Gun->GetRightVector() * 1000.f),
	                                         ECC_Visibility))
	{
		int SectionIndex;

		UMaterialInterface* Hit_material = HitResult.Component->GetMaterialFromCollisionFaceIndex(0, SectionIndex);

		if (Hit_material == switchable_mat)
		{
			HitResult.Component->SetMaterial(0, mirror_mat);
		}
	}
}

void AFP_FirstPersonCharacter::LoadBullet()
{
	Bullet_Size_Alpha = 0;
	const FAttachmentTransformRules AttachmentTransformRules{
		EAttachmentRule::SnapToTarget, EAttachmentRule::KeepWorld, EAttachmentRule::KeepWorld, false
	};
	Bullets_pool[current_bullet]->AttachToComponent(BulletLocation, AttachmentTransformRules);
	Bullets_pool[current_bullet]->CollisionSphere->SetSimulatePhysics(false);
	Bullets_pool[current_bullet]->SetActorTickEnabled(true);
	Bullets_pool[current_bullet]->SetActorHiddenInGame(false);
	Bullets_pool[current_bullet]->SetActorEnableCollision(true);
	Bullets_pool[current_bullet]->ResetGraphics();

	Fire2_IsPressed = true;
}

void AFP_FirstPersonCharacter::ChargeBullet(int dir)
{
	Bullet_Size_Alpha += FApp::GetDeltaTime() * dir;
	float Alpha = UKismetMathLibrary::FClamp(Bullet_Size_Alpha, 0, BulletScaleSpeed) / BulletScaleSpeed;
	Bullets_pool[current_bullet]->Grow(Alpha);
	if (Alpha >= 1 && dir > 0)
	{
		ParticleSystemComponent->Activate();
		Bullet_Charged = true;
	}
	else if (dir < 0 && ParticleSystemComponent->IsActive())
	{
		ParticleSystemComponent->Deactivate();
	}
}


void AFP_FirstPersonCharacter::SetGrabbedObject(UPrimitiveComponent* ObjectToGrab, AActor* ActorToGrab)
{
	GrabbedObject = ObjectToGrab;
	GrabbedLaser = Cast<ALaserActorBase>(ActorToGrab);

	if (GrabbedObject)
	{
		GrabbedObject->SetSimulatePhysics(false);
		GrabbedObject->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		GrabbedObject->AttachToComponent(GrabbedObjectLocation,
		                                 FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	}
}


void AFP_FirstPersonCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// Add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AFP_FirstPersonCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// Add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AFP_FirstPersonCharacter::TurnAtRate(float Rate)
{
	// Calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AFP_FirstPersonCharacter::LookUpAtRate(float Rate)
{
	// Calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

FHitResult AFP_FirstPersonCharacter::WeaponTrace(const FVector& StartTrace, const FVector& EndTrace) const
{
	// Perform trace to retrieve hit info
	FCollisionQueryParams TraceParams(SCENE_QUERY_STAT(WeaponTrace), true, GetInstigator());
	TraceParams.bReturnPhysicalMaterial = true;

	FHitResult Hit(ForceInit);
	GetWorld()->LineTraceSingleByChannel(Hit, StartTrace, EndTrace, COLLISION_WEAPON, TraceParams);

	return Hit;
}
