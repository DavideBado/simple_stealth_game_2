// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Bullet.h"
#include "Perception/PawnSensingComponent.h"
#include "FP_FirstPersonCharacter.generated.h"

class UInputComponent;
class UCameraComponent;
class USkeletalMeshComponent;
class USoundBase;
class UAnimMontage;
class ALaserActorBase;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FUIDelegate);

UCLASS(config=Game)
class AFP_FirstPersonCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	USkeletalMeshComponent* Mesh1P;

	/** Gun mesh */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* FP_Gun;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FirstPersonCameraComponent;

public:
	AFP_FirstPersonCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	USoundBase* FireSound;

	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	UAnimMontage* FireAnimation;

	UPROPERTY(VisibleAnywhere, Category= Components)
	USceneComponent* GrabbedObjectLocation;
	UPROPERTY(VisibleAnywhere, Category= Components)
	USceneComponent* BulletLocation;

	UPROPERTY(EditDefaultsOnly, Category= Components)
	float MinSize;
	UPROPERTY(EditDefaultsOnly, Category= Components)
	float MaxSize;

	UPROPERTY(EditDefaultsOnly, Category= Components)
	float BulletScaleSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<ECollisionChannel> cast_channel;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<ECollisionChannel> Laser_Coll_channel;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterial* mirror_mat;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterial* switchable_mat;

	bool Alive = true;
	void Die();
	void ResetGrabbedLaser();
private:
	bool Fire2_IsPressed;
	bool Bullet_Charged;
	float Bullet_Size_Alpha;
	UPROPERTY(VisibleAnywhere, Instanced)
	UParticleSystemComponent* ParticleSystemComponent;
	UPROPERTY()
	UParticleSystem* FullEnergyParticle;
protected:
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintAssignable, Category = "UI")
	FUIDelegate LoadGameOverUI;

	UPROPERTY(EditAnyWhere, BluePrintReadWrite, Category = SaveSystem)
	TSubclassOf<class ABullet> EnergyBullet;
	UPROPERTY()
	TArray<ABullet*> Bullets_pool;

	int Pool_size = 3;
	int current_bullet;
	/** Fires a virtual projectile. */
	void OnFire();
	void EndFire(float Strength);
	void LowFire();
	void LoadBullet();
	void ChargeBullet(int dir);
	void ShootBullet();
	void Select();
	void Switch();
	void SetGrabbedObject(UPrimitiveComponent* ObjectToGrab, AActor* ActorToGrab);

	UPROPERTY()
	UPrimitiveComponent* GrabbedObject;
	UPROPERTY()
	ALaserActorBase* GrabbedLaser;

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles strafing movement, left and right */
	void MoveRight(float Val);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/* 
	 * Performs a trace between two points
	 * 
	 * @param	StartTrace	Trace starting point
	 * @param	EndTrac		Trace end point
	 * @returns FHitResult returns a struct containing trace result - who/what the trace hit etc.
	 */
	FHitResult WeaponTrace(const FVector& StartTrace, const FVector& EndTrace) const;

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface

	virtual void Tick(float DeltaSeconds) override;

public:
	/** Returns Mesh1P subobject **/
	FORCEINLINE class USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }
};
