// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Simple_stealth_game/Public/MyActionTriggerBox.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyActionTriggerBox() {}
// Cross Module References
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_AMyActionTriggerBox_NoRegister();
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_AMyActionTriggerBox();
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_AMyEnDisableActorsTriggerBox();
	UPackage* Z_Construct_UPackage__Script_Simple_stealth_game();
	ENGINE_API UClass* Z_Construct_UClass_ATriggerBox_NoRegister();
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_AMyEventActor_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(AMyActionTriggerBox::execOnInput)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnInput();
		P_NATIVE_END;
	}
	void AMyActionTriggerBox::StaticRegisterNativesAMyActionTriggerBox()
	{
		UClass* Class = AMyActionTriggerBox::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnInput", &AMyActionTriggerBox::execOnInput },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AMyActionTriggerBox_OnInput_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMyActionTriggerBox_OnInput_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MyActionTriggerBox.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMyActionTriggerBox_OnInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMyActionTriggerBox, nullptr, "OnInput", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMyActionTriggerBox_OnInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMyActionTriggerBox_OnInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMyActionTriggerBox_OnInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMyActionTriggerBox_OnInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AMyActionTriggerBox_NoRegister()
	{
		return AMyActionTriggerBox::StaticClass();
	}
	struct Z_Construct_UClass_AMyActionTriggerBox_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnInputTriggers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OnInputTriggers;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OnInputTriggers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventActors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_EventActors;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EventActors_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMyActionTriggerBox_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AMyEnDisableActorsTriggerBox,
		(UObject* (*)())Z_Construct_UPackage__Script_Simple_stealth_game,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AMyActionTriggerBox_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AMyActionTriggerBox_OnInput, "OnInput" }, // 4028208816
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyActionTriggerBox_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "MyActionTriggerBox.h" },
		{ "ModuleRelativePath", "Public/MyActionTriggerBox.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyActionTriggerBox_Statics::NewProp_OnInputTriggers_MetaData[] = {
		{ "Category", "MyActionTriggerBox" },
		{ "ModuleRelativePath", "Public/MyActionTriggerBox.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AMyActionTriggerBox_Statics::NewProp_OnInputTriggers = { "OnInputTriggers", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMyActionTriggerBox, OnInputTriggers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AMyActionTriggerBox_Statics::NewProp_OnInputTriggers_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMyActionTriggerBox_Statics::NewProp_OnInputTriggers_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMyActionTriggerBox_Statics::NewProp_OnInputTriggers_Inner = { "OnInputTriggers", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ATriggerBox_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyActionTriggerBox_Statics::NewProp_EventActors_MetaData[] = {
		{ "Category", "MyActionTriggerBox" },
		{ "ModuleRelativePath", "Public/MyActionTriggerBox.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AMyActionTriggerBox_Statics::NewProp_EventActors = { "EventActors", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMyActionTriggerBox, EventActors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AMyActionTriggerBox_Statics::NewProp_EventActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMyActionTriggerBox_Statics::NewProp_EventActors_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMyActionTriggerBox_Statics::NewProp_EventActors_Inner = { "EventActors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AMyEventActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMyActionTriggerBox_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyActionTriggerBox_Statics::NewProp_OnInputTriggers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyActionTriggerBox_Statics::NewProp_OnInputTriggers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyActionTriggerBox_Statics::NewProp_EventActors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyActionTriggerBox_Statics::NewProp_EventActors_Inner,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMyActionTriggerBox_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMyActionTriggerBox>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMyActionTriggerBox_Statics::ClassParams = {
		&AMyActionTriggerBox::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AMyActionTriggerBox_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AMyActionTriggerBox_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMyActionTriggerBox_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMyActionTriggerBox_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMyActionTriggerBox()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMyActionTriggerBox_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMyActionTriggerBox, 3664922563);
	template<> SIMPLE_STEALTH_GAME_API UClass* StaticClass<AMyActionTriggerBox>()
	{
		return AMyActionTriggerBox::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMyActionTriggerBox(Z_Construct_UClass_AMyActionTriggerBox, &AMyActionTriggerBox::StaticClass, TEXT("/Script/Simple_stealth_game"), TEXT("AMyActionTriggerBox"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMyActionTriggerBox);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
