// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SIMPLE_STEALTH_GAME_LaserEmitter_generated_h
#error "LaserEmitter.generated.h already included, missing '#pragma once' in LaserEmitter.h"
#endif
#define SIMPLE_STEALTH_GAME_LaserEmitter_generated_h

#define Simple_stealth_game_Source_Simple_stealth_game_Public_LaserEmitter_h_14_SPARSE_DATA
#define Simple_stealth_game_Source_Simple_stealth_game_Public_LaserEmitter_h_14_RPC_WRAPPERS
#define Simple_stealth_game_Source_Simple_stealth_game_Public_LaserEmitter_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Simple_stealth_game_Source_Simple_stealth_game_Public_LaserEmitter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesALaserEmitter(); \
	friend struct Z_Construct_UClass_ALaserEmitter_Statics; \
public: \
	DECLARE_CLASS(ALaserEmitter, ALaserActorBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Simple_stealth_game"), NO_API) \
	DECLARE_SERIALIZER(ALaserEmitter)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_LaserEmitter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesALaserEmitter(); \
	friend struct Z_Construct_UClass_ALaserEmitter_Statics; \
public: \
	DECLARE_CLASS(ALaserEmitter, ALaserActorBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Simple_stealth_game"), NO_API) \
	DECLARE_SERIALIZER(ALaserEmitter)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_LaserEmitter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALaserEmitter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALaserEmitter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALaserEmitter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALaserEmitter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALaserEmitter(ALaserEmitter&&); \
	NO_API ALaserEmitter(const ALaserEmitter&); \
public:


#define Simple_stealth_game_Source_Simple_stealth_game_Public_LaserEmitter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALaserEmitter(ALaserEmitter&&); \
	NO_API ALaserEmitter(const ALaserEmitter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALaserEmitter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALaserEmitter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ALaserEmitter)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_LaserEmitter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__beams() { return STRUCT_OFFSET(ALaserEmitter, beams); }


#define Simple_stealth_game_Source_Simple_stealth_game_Public_LaserEmitter_h_11_PROLOG
#define Simple_stealth_game_Source_Simple_stealth_game_Public_LaserEmitter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Simple_stealth_game_Source_Simple_stealth_game_Public_LaserEmitter_h_14_PRIVATE_PROPERTY_OFFSET \
	Simple_stealth_game_Source_Simple_stealth_game_Public_LaserEmitter_h_14_SPARSE_DATA \
	Simple_stealth_game_Source_Simple_stealth_game_Public_LaserEmitter_h_14_RPC_WRAPPERS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_LaserEmitter_h_14_INCLASS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_LaserEmitter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Simple_stealth_game_Source_Simple_stealth_game_Public_LaserEmitter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Simple_stealth_game_Source_Simple_stealth_game_Public_LaserEmitter_h_14_PRIVATE_PROPERTY_OFFSET \
	Simple_stealth_game_Source_Simple_stealth_game_Public_LaserEmitter_h_14_SPARSE_DATA \
	Simple_stealth_game_Source_Simple_stealth_game_Public_LaserEmitter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_LaserEmitter_h_14_INCLASS_NO_PURE_DECLS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_LaserEmitter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SIMPLE_STEALTH_GAME_API UClass* StaticClass<class ALaserEmitter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Simple_stealth_game_Source_Simple_stealth_game_Public_LaserEmitter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
