// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Simple_stealth_game/Public/LaserEmitter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLaserEmitter() {}
// Cross Module References
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_ALaserEmitter_NoRegister();
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_ALaserEmitter();
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_ALaserActorBase();
	UPackage* Z_Construct_UPackage__Script_Simple_stealth_game();
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_ALaser_beam_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterial_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_UMeshComponent_NoRegister();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_ECollisionChannel();
// End Cross Module References
	void ALaserEmitter::StaticRegisterNativesALaserEmitter()
	{
	}
	UClass* Z_Construct_UClass_ALaserEmitter_NoRegister()
	{
		return ALaserEmitter::StaticClass();
	}
	struct Z_Construct_UClass_ALaserEmitter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_beams_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_beams;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_beams_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_cast_distance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_cast_distance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_sensor_mat_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_sensor_mat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_mirror_mat_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_mirror_mat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_laserbeam_Pref_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_laserbeam_Pref;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_emitterComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_emitterComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_cast_channel_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_cast_channel;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ALaserEmitter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ALaserActorBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Simple_stealth_game,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALaserEmitter_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "LaserEmitter.h" },
		{ "ModuleRelativePath", "Public/LaserEmitter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALaserEmitter_Statics::NewProp_beams_MetaData[] = {
		{ "ModuleRelativePath", "Public/LaserEmitter.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ALaserEmitter_Statics::NewProp_beams = { "beams", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALaserEmitter, beams), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ALaserEmitter_Statics::NewProp_beams_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALaserEmitter_Statics::NewProp_beams_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ALaserEmitter_Statics::NewProp_beams_Inner = { "beams", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ALaser_beam_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALaserEmitter_Statics::NewProp_cast_distance_MetaData[] = {
		{ "Category", "LaserEmitter" },
		{ "ModuleRelativePath", "Public/LaserEmitter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ALaserEmitter_Statics::NewProp_cast_distance = { "cast_distance", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALaserEmitter, cast_distance), METADATA_PARAMS(Z_Construct_UClass_ALaserEmitter_Statics::NewProp_cast_distance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALaserEmitter_Statics::NewProp_cast_distance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALaserEmitter_Statics::NewProp_sensor_mat_MetaData[] = {
		{ "Category", "LaserEmitter" },
		{ "ModuleRelativePath", "Public/LaserEmitter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ALaserEmitter_Statics::NewProp_sensor_mat = { "sensor_mat", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALaserEmitter, sensor_mat), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ALaserEmitter_Statics::NewProp_sensor_mat_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALaserEmitter_Statics::NewProp_sensor_mat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALaserEmitter_Statics::NewProp_mirror_mat_MetaData[] = {
		{ "Category", "LaserEmitter" },
		{ "ModuleRelativePath", "Public/LaserEmitter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ALaserEmitter_Statics::NewProp_mirror_mat = { "mirror_mat", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALaserEmitter, mirror_mat), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ALaserEmitter_Statics::NewProp_mirror_mat_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALaserEmitter_Statics::NewProp_mirror_mat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALaserEmitter_Statics::NewProp_laserbeam_Pref_MetaData[] = {
		{ "Category", "LaserEmitter" },
		{ "ModuleRelativePath", "Public/LaserEmitter.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ALaserEmitter_Statics::NewProp_laserbeam_Pref = { "laserbeam_Pref", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALaserEmitter, laserbeam_Pref), Z_Construct_UClass_ALaser_beam_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ALaserEmitter_Statics::NewProp_laserbeam_Pref_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALaserEmitter_Statics::NewProp_laserbeam_Pref_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALaserEmitter_Statics::NewProp_emitterComponent_MetaData[] = {
		{ "Category", "LaserEmitter" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/LaserEmitter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ALaserEmitter_Statics::NewProp_emitterComponent = { "emitterComponent", nullptr, (EPropertyFlags)0x001000000008000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALaserEmitter, emitterComponent), Z_Construct_UClass_UMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ALaserEmitter_Statics::NewProp_emitterComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALaserEmitter_Statics::NewProp_emitterComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALaserEmitter_Statics::NewProp_cast_channel_MetaData[] = {
		{ "Category", "LaserEmitter" },
		{ "ModuleRelativePath", "Public/LaserEmitter.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_ALaserEmitter_Statics::NewProp_cast_channel = { "cast_channel", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALaserEmitter, cast_channel), Z_Construct_UEnum_Engine_ECollisionChannel, METADATA_PARAMS(Z_Construct_UClass_ALaserEmitter_Statics::NewProp_cast_channel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALaserEmitter_Statics::NewProp_cast_channel_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ALaserEmitter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALaserEmitter_Statics::NewProp_beams,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALaserEmitter_Statics::NewProp_beams_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALaserEmitter_Statics::NewProp_cast_distance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALaserEmitter_Statics::NewProp_sensor_mat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALaserEmitter_Statics::NewProp_mirror_mat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALaserEmitter_Statics::NewProp_laserbeam_Pref,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALaserEmitter_Statics::NewProp_emitterComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALaserEmitter_Statics::NewProp_cast_channel,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ALaserEmitter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ALaserEmitter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ALaserEmitter_Statics::ClassParams = {
		&ALaserEmitter::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ALaserEmitter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ALaserEmitter_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ALaserEmitter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ALaserEmitter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ALaserEmitter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ALaserEmitter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ALaserEmitter, 855278219);
	template<> SIMPLE_STEALTH_GAME_API UClass* StaticClass<ALaserEmitter>()
	{
		return ALaserEmitter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ALaserEmitter(Z_Construct_UClass_ALaserEmitter, &ALaserEmitter::StaticClass, TEXT("/Script/Simple_stealth_game"), TEXT("ALaserEmitter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ALaserEmitter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
