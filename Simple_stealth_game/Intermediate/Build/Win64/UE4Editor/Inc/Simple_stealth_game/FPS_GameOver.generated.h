// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SIMPLE_STEALTH_GAME_FPS_GameOver_generated_h
#error "FPS_GameOver.generated.h already included, missing '#pragma once' in FPS_GameOver.h"
#endif
#define SIMPLE_STEALTH_GAME_FPS_GameOver_generated_h

#define Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_GameOver_h_15_SPARSE_DATA
#define Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_GameOver_h_15_RPC_WRAPPERS
#define Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_GameOver_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_GameOver_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFPS_GameOver(); \
	friend struct Z_Construct_UClass_UFPS_GameOver_Statics; \
public: \
	DECLARE_CLASS(UFPS_GameOver, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Simple_stealth_game"), NO_API) \
	DECLARE_SERIALIZER(UFPS_GameOver)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_GameOver_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUFPS_GameOver(); \
	friend struct Z_Construct_UClass_UFPS_GameOver_Statics; \
public: \
	DECLARE_CLASS(UFPS_GameOver, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Simple_stealth_game"), NO_API) \
	DECLARE_SERIALIZER(UFPS_GameOver)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_GameOver_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFPS_GameOver(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFPS_GameOver) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFPS_GameOver); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFPS_GameOver); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFPS_GameOver(UFPS_GameOver&&); \
	NO_API UFPS_GameOver(const UFPS_GameOver&); \
public:


#define Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_GameOver_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFPS_GameOver(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFPS_GameOver(UFPS_GameOver&&); \
	NO_API UFPS_GameOver(const UFPS_GameOver&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFPS_GameOver); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFPS_GameOver); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFPS_GameOver)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_GameOver_h_15_PRIVATE_PROPERTY_OFFSET
#define Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_GameOver_h_12_PROLOG
#define Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_GameOver_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_GameOver_h_15_PRIVATE_PROPERTY_OFFSET \
	Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_GameOver_h_15_SPARSE_DATA \
	Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_GameOver_h_15_RPC_WRAPPERS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_GameOver_h_15_INCLASS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_GameOver_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_GameOver_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_GameOver_h_15_PRIVATE_PROPERTY_OFFSET \
	Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_GameOver_h_15_SPARSE_DATA \
	Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_GameOver_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_GameOver_h_15_INCLASS_NO_PURE_DECLS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_GameOver_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SIMPLE_STEALTH_GAME_API UClass* StaticClass<class UFPS_GameOver>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Simple_stealth_game_Source_Simple_stealth_game_Public_FPS_GameOver_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
