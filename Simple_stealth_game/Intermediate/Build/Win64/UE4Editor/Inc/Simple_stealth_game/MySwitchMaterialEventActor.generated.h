// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SIMPLE_STEALTH_GAME_MySwitchMaterialEventActor_generated_h
#error "MySwitchMaterialEventActor.generated.h already included, missing '#pragma once' in MySwitchMaterialEventActor.h"
#endif
#define SIMPLE_STEALTH_GAME_MySwitchMaterialEventActor_generated_h

#define Simple_stealth_game_Source_Simple_stealth_game_Public_MySwitchMaterialEventActor_h_15_SPARSE_DATA
#define Simple_stealth_game_Source_Simple_stealth_game_Public_MySwitchMaterialEventActor_h_15_RPC_WRAPPERS
#define Simple_stealth_game_Source_Simple_stealth_game_Public_MySwitchMaterialEventActor_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Simple_stealth_game_Source_Simple_stealth_game_Public_MySwitchMaterialEventActor_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMySwitchMaterialEventActor(); \
	friend struct Z_Construct_UClass_AMySwitchMaterialEventActor_Statics; \
public: \
	DECLARE_CLASS(AMySwitchMaterialEventActor, AMyEventActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Simple_stealth_game"), NO_API) \
	DECLARE_SERIALIZER(AMySwitchMaterialEventActor)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_MySwitchMaterialEventActor_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAMySwitchMaterialEventActor(); \
	friend struct Z_Construct_UClass_AMySwitchMaterialEventActor_Statics; \
public: \
	DECLARE_CLASS(AMySwitchMaterialEventActor, AMyEventActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Simple_stealth_game"), NO_API) \
	DECLARE_SERIALIZER(AMySwitchMaterialEventActor)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_MySwitchMaterialEventActor_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMySwitchMaterialEventActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMySwitchMaterialEventActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMySwitchMaterialEventActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMySwitchMaterialEventActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMySwitchMaterialEventActor(AMySwitchMaterialEventActor&&); \
	NO_API AMySwitchMaterialEventActor(const AMySwitchMaterialEventActor&); \
public:


#define Simple_stealth_game_Source_Simple_stealth_game_Public_MySwitchMaterialEventActor_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMySwitchMaterialEventActor() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMySwitchMaterialEventActor(AMySwitchMaterialEventActor&&); \
	NO_API AMySwitchMaterialEventActor(const AMySwitchMaterialEventActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMySwitchMaterialEventActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMySwitchMaterialEventActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMySwitchMaterialEventActor)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_MySwitchMaterialEventActor_h_15_PRIVATE_PROPERTY_OFFSET
#define Simple_stealth_game_Source_Simple_stealth_game_Public_MySwitchMaterialEventActor_h_12_PROLOG
#define Simple_stealth_game_Source_Simple_stealth_game_Public_MySwitchMaterialEventActor_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MySwitchMaterialEventActor_h_15_PRIVATE_PROPERTY_OFFSET \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MySwitchMaterialEventActor_h_15_SPARSE_DATA \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MySwitchMaterialEventActor_h_15_RPC_WRAPPERS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MySwitchMaterialEventActor_h_15_INCLASS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MySwitchMaterialEventActor_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Simple_stealth_game_Source_Simple_stealth_game_Public_MySwitchMaterialEventActor_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MySwitchMaterialEventActor_h_15_PRIVATE_PROPERTY_OFFSET \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MySwitchMaterialEventActor_h_15_SPARSE_DATA \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MySwitchMaterialEventActor_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MySwitchMaterialEventActor_h_15_INCLASS_NO_PURE_DECLS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MySwitchMaterialEventActor_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SIMPLE_STEALTH_GAME_API UClass* StaticClass<class AMySwitchMaterialEventActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Simple_stealth_game_Source_Simple_stealth_game_Public_MySwitchMaterialEventActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
