// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SIMPLE_STEALTH_GAME_BorderTriggerBox_generated_h
#error "BorderTriggerBox.generated.h already included, missing '#pragma once' in BorderTriggerBox.h"
#endif
#define SIMPLE_STEALTH_GAME_BorderTriggerBox_generated_h

#define Simple_stealth_game_Source_Simple_stealth_game_Public_BorderTriggerBox_h_15_SPARSE_DATA
#define Simple_stealth_game_Source_Simple_stealth_game_Public_BorderTriggerBox_h_15_RPC_WRAPPERS
#define Simple_stealth_game_Source_Simple_stealth_game_Public_BorderTriggerBox_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Simple_stealth_game_Source_Simple_stealth_game_Public_BorderTriggerBox_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABorderTriggerBox(); \
	friend struct Z_Construct_UClass_ABorderTriggerBox_Statics; \
public: \
	DECLARE_CLASS(ABorderTriggerBox, ATriggerBox, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Simple_stealth_game"), NO_API) \
	DECLARE_SERIALIZER(ABorderTriggerBox)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_BorderTriggerBox_h_15_INCLASS \
private: \
	static void StaticRegisterNativesABorderTriggerBox(); \
	friend struct Z_Construct_UClass_ABorderTriggerBox_Statics; \
public: \
	DECLARE_CLASS(ABorderTriggerBox, ATriggerBox, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Simple_stealth_game"), NO_API) \
	DECLARE_SERIALIZER(ABorderTriggerBox)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_BorderTriggerBox_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABorderTriggerBox(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABorderTriggerBox) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABorderTriggerBox); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABorderTriggerBox); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABorderTriggerBox(ABorderTriggerBox&&); \
	NO_API ABorderTriggerBox(const ABorderTriggerBox&); \
public:


#define Simple_stealth_game_Source_Simple_stealth_game_Public_BorderTriggerBox_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABorderTriggerBox(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABorderTriggerBox(ABorderTriggerBox&&); \
	NO_API ABorderTriggerBox(const ABorderTriggerBox&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABorderTriggerBox); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABorderTriggerBox); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABorderTriggerBox)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_BorderTriggerBox_h_15_PRIVATE_PROPERTY_OFFSET
#define Simple_stealth_game_Source_Simple_stealth_game_Public_BorderTriggerBox_h_12_PROLOG
#define Simple_stealth_game_Source_Simple_stealth_game_Public_BorderTriggerBox_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Simple_stealth_game_Source_Simple_stealth_game_Public_BorderTriggerBox_h_15_PRIVATE_PROPERTY_OFFSET \
	Simple_stealth_game_Source_Simple_stealth_game_Public_BorderTriggerBox_h_15_SPARSE_DATA \
	Simple_stealth_game_Source_Simple_stealth_game_Public_BorderTriggerBox_h_15_RPC_WRAPPERS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_BorderTriggerBox_h_15_INCLASS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_BorderTriggerBox_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Simple_stealth_game_Source_Simple_stealth_game_Public_BorderTriggerBox_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Simple_stealth_game_Source_Simple_stealth_game_Public_BorderTriggerBox_h_15_PRIVATE_PROPERTY_OFFSET \
	Simple_stealth_game_Source_Simple_stealth_game_Public_BorderTriggerBox_h_15_SPARSE_DATA \
	Simple_stealth_game_Source_Simple_stealth_game_Public_BorderTriggerBox_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_BorderTriggerBox_h_15_INCLASS_NO_PURE_DECLS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_BorderTriggerBox_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SIMPLE_STEALTH_GAME_API UClass* StaticClass<class ABorderTriggerBox>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Simple_stealth_game_Source_Simple_stealth_game_Public_BorderTriggerBox_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
