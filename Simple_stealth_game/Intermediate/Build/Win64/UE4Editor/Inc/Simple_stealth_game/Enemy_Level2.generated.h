// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class APawn;
#ifdef SIMPLE_STEALTH_GAME_Enemy_Level2_generated_h
#error "Enemy_Level2.generated.h already included, missing '#pragma once' in Enemy_Level2.h"
#endif
#define SIMPLE_STEALTH_GAME_Enemy_Level2_generated_h

#define Simple_stealth_game_Source_Simple_stealth_game_Public_Enemy_Level2_h_15_SPARSE_DATA
#define Simple_stealth_game_Source_Simple_stealth_game_Public_Enemy_Level2_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnPawnSeen);


#define Simple_stealth_game_Source_Simple_stealth_game_Public_Enemy_Level2_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnPawnSeen);


#define Simple_stealth_game_Source_Simple_stealth_game_Public_Enemy_Level2_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEnemy_Level2(); \
	friend struct Z_Construct_UClass_AEnemy_Level2_Statics; \
public: \
	DECLARE_CLASS(AEnemy_Level2, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Simple_stealth_game"), NO_API) \
	DECLARE_SERIALIZER(AEnemy_Level2)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_Enemy_Level2_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAEnemy_Level2(); \
	friend struct Z_Construct_UClass_AEnemy_Level2_Statics; \
public: \
	DECLARE_CLASS(AEnemy_Level2, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Simple_stealth_game"), NO_API) \
	DECLARE_SERIALIZER(AEnemy_Level2)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_Enemy_Level2_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEnemy_Level2(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEnemy_Level2) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemy_Level2); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemy_Level2); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemy_Level2(AEnemy_Level2&&); \
	NO_API AEnemy_Level2(const AEnemy_Level2&); \
public:


#define Simple_stealth_game_Source_Simple_stealth_game_Public_Enemy_Level2_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemy_Level2(AEnemy_Level2&&); \
	NO_API AEnemy_Level2(const AEnemy_Level2&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemy_Level2); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemy_Level2); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AEnemy_Level2)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_Enemy_Level2_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__BodyMaterial() { return STRUCT_OFFSET(AEnemy_Level2, BodyMaterial); } \
	FORCEINLINE static uint32 __PPO__PawnSensingComp() { return STRUCT_OFFSET(AEnemy_Level2, PawnSensingComp); } \
	FORCEINLINE static uint32 __PPO__player() { return STRUCT_OFFSET(AEnemy_Level2, player); }


#define Simple_stealth_game_Source_Simple_stealth_game_Public_Enemy_Level2_h_12_PROLOG
#define Simple_stealth_game_Source_Simple_stealth_game_Public_Enemy_Level2_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Simple_stealth_game_Source_Simple_stealth_game_Public_Enemy_Level2_h_15_PRIVATE_PROPERTY_OFFSET \
	Simple_stealth_game_Source_Simple_stealth_game_Public_Enemy_Level2_h_15_SPARSE_DATA \
	Simple_stealth_game_Source_Simple_stealth_game_Public_Enemy_Level2_h_15_RPC_WRAPPERS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_Enemy_Level2_h_15_INCLASS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_Enemy_Level2_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Simple_stealth_game_Source_Simple_stealth_game_Public_Enemy_Level2_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Simple_stealth_game_Source_Simple_stealth_game_Public_Enemy_Level2_h_15_PRIVATE_PROPERTY_OFFSET \
	Simple_stealth_game_Source_Simple_stealth_game_Public_Enemy_Level2_h_15_SPARSE_DATA \
	Simple_stealth_game_Source_Simple_stealth_game_Public_Enemy_Level2_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_Enemy_Level2_h_15_INCLASS_NO_PURE_DECLS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_Enemy_Level2_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SIMPLE_STEALTH_GAME_API UClass* StaticClass<class AEnemy_Level2>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Simple_stealth_game_Source_Simple_stealth_game_Public_Enemy_Level2_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
