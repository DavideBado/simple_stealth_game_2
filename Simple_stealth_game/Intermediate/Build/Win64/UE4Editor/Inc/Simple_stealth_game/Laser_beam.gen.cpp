// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Simple_stealth_game/Public/Laser_beam.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLaser_beam() {}
// Cross Module References
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_ALaser_beam_NoRegister();
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_ALaser_beam();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Simple_stealth_game();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	void ALaser_beam::StaticRegisterNativesALaser_beam()
	{
	}
	UClass* Z_Construct_UClass_ALaser_beam_NoRegister()
	{
		return ALaser_beam::StaticClass();
	}
	struct Z_Construct_UClass_ALaser_beam_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Material_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Material;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_static_mesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_static_mesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ALaser_beam_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Simple_stealth_game,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALaser_beam_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Laser_beam.h" },
		{ "ModuleRelativePath", "Public/Laser_beam.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALaser_beam_Statics::NewProp_Material_MetaData[] = {
		{ "ModuleRelativePath", "Public/Laser_beam.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ALaser_beam_Statics::NewProp_Material = { "Material", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALaser_beam, Material), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ALaser_beam_Statics::NewProp_Material_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALaser_beam_Statics::NewProp_Material_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALaser_beam_Statics::NewProp_static_mesh_MetaData[] = {
		{ "Category", "Laser_beam" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Laser_beam.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ALaser_beam_Statics::NewProp_static_mesh = { "static_mesh", nullptr, (EPropertyFlags)0x001000000008000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ALaser_beam, static_mesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ALaser_beam_Statics::NewProp_static_mesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ALaser_beam_Statics::NewProp_static_mesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ALaser_beam_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALaser_beam_Statics::NewProp_Material,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ALaser_beam_Statics::NewProp_static_mesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ALaser_beam_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ALaser_beam>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ALaser_beam_Statics::ClassParams = {
		&ALaser_beam::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ALaser_beam_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ALaser_beam_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ALaser_beam_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ALaser_beam_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ALaser_beam()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ALaser_beam_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ALaser_beam, 3975886112);
	template<> SIMPLE_STEALTH_GAME_API UClass* StaticClass<ALaser_beam>()
	{
		return ALaser_beam::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ALaser_beam(Z_Construct_UClass_ALaser_beam, &ALaser_beam::StaticClass, TEXT("/Script/Simple_stealth_game"), TEXT("ALaser_beam"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ALaser_beam);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
