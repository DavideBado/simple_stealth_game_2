// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Simple_stealth_game/Public/MyEventActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyEventActor() {}
// Cross Module References
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_AMyEventActor_NoRegister();
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_AMyEventActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Simple_stealth_game();
// End Cross Module References
	void AMyEventActor::StaticRegisterNativesAMyEventActor()
	{
	}
	UClass* Z_Construct_UClass_AMyEventActor_NoRegister()
	{
		return AMyEventActor::StaticClass();
	}
	struct Z_Construct_UClass_AMyEventActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMyEventActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Simple_stealth_game,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyEventActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MyEventActor.h" },
		{ "ModuleRelativePath", "Public/MyEventActor.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMyEventActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMyEventActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMyEventActor_Statics::ClassParams = {
		&AMyEventActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMyEventActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMyEventActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMyEventActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMyEventActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMyEventActor, 514312123);
	template<> SIMPLE_STEALTH_GAME_API UClass* StaticClass<AMyEventActor>()
	{
		return AMyEventActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMyEventActor(Z_Construct_UClass_AMyEventActor, &AMyEventActor::StaticClass, TEXT("/Script/Simple_stealth_game"), TEXT("AMyEventActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMyEventActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
