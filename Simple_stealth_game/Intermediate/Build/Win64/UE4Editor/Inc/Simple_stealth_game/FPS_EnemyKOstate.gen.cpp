// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Simple_stealth_game/Public/FPS_EnemyKOstate.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFPS_EnemyKOstate() {}
// Cross Module References
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_UFPS_EnemyKOstate_NoRegister();
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_UFPS_EnemyKOstate();
	ENGINE_API UClass* Z_Construct_UClass_UAnimNotifyState();
	UPackage* Z_Construct_UPackage__Script_Simple_stealth_game();
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_AEnemy_Level2_NoRegister();
// End Cross Module References
	void UFPS_EnemyKOstate::StaticRegisterNativesUFPS_EnemyKOstate()
	{
	}
	UClass* Z_Construct_UClass_UFPS_EnemyKOstate_NoRegister()
	{
		return UFPS_EnemyKOstate::StaticClass();
	}
	struct Z_Construct_UClass_UFPS_EnemyKOstate_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_enemy_owner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_enemy_owner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFPS_EnemyKOstate_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAnimNotifyState,
		(UObject* (*)())Z_Construct_UPackage__Script_Simple_stealth_game,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFPS_EnemyKOstate_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "FPS_EnemyKOstate.h" },
		{ "ModuleRelativePath", "Public/FPS_EnemyKOstate.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFPS_EnemyKOstate_Statics::NewProp_enemy_owner_MetaData[] = {
		{ "ModuleRelativePath", "Public/FPS_EnemyKOstate.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UFPS_EnemyKOstate_Statics::NewProp_enemy_owner = { "enemy_owner", nullptr, (EPropertyFlags)0x0040000000000010, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFPS_EnemyKOstate, enemy_owner), Z_Construct_UClass_AEnemy_Level2_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UFPS_EnemyKOstate_Statics::NewProp_enemy_owner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFPS_EnemyKOstate_Statics::NewProp_enemy_owner_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFPS_EnemyKOstate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFPS_EnemyKOstate_Statics::NewProp_enemy_owner,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFPS_EnemyKOstate_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFPS_EnemyKOstate>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFPS_EnemyKOstate_Statics::ClassParams = {
		&UFPS_EnemyKOstate::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFPS_EnemyKOstate_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFPS_EnemyKOstate_Statics::PropPointers),
		0,
		0x001130A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFPS_EnemyKOstate_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFPS_EnemyKOstate_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFPS_EnemyKOstate()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFPS_EnemyKOstate_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFPS_EnemyKOstate, 3258228401);
	template<> SIMPLE_STEALTH_GAME_API UClass* StaticClass<UFPS_EnemyKOstate>()
	{
		return UFPS_EnemyKOstate::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFPS_EnemyKOstate(Z_Construct_UClass_UFPS_EnemyKOstate, &UFPS_EnemyKOstate::StaticClass, TEXT("/Script/Simple_stealth_game"), TEXT("UFPS_EnemyKOstate"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFPS_EnemyKOstate);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
