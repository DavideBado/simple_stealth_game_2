// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SIMPLE_STEALTH_GAME_MyActionTriggerBox_generated_h
#error "MyActionTriggerBox.generated.h already included, missing '#pragma once' in MyActionTriggerBox.h"
#endif
#define SIMPLE_STEALTH_GAME_MyActionTriggerBox_generated_h

#define Simple_stealth_game_Source_Simple_stealth_game_Public_MyActionTriggerBox_h_17_SPARSE_DATA
#define Simple_stealth_game_Source_Simple_stealth_game_Public_MyActionTriggerBox_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnInput);


#define Simple_stealth_game_Source_Simple_stealth_game_Public_MyActionTriggerBox_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnInput);


#define Simple_stealth_game_Source_Simple_stealth_game_Public_MyActionTriggerBox_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyActionTriggerBox(); \
	friend struct Z_Construct_UClass_AMyActionTriggerBox_Statics; \
public: \
	DECLARE_CLASS(AMyActionTriggerBox, AMyEnDisableActorsTriggerBox, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Simple_stealth_game"), NO_API) \
	DECLARE_SERIALIZER(AMyActionTriggerBox)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_MyActionTriggerBox_h_17_INCLASS \
private: \
	static void StaticRegisterNativesAMyActionTriggerBox(); \
	friend struct Z_Construct_UClass_AMyActionTriggerBox_Statics; \
public: \
	DECLARE_CLASS(AMyActionTriggerBox, AMyEnDisableActorsTriggerBox, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Simple_stealth_game"), NO_API) \
	DECLARE_SERIALIZER(AMyActionTriggerBox)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_MyActionTriggerBox_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyActionTriggerBox(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyActionTriggerBox) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyActionTriggerBox); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyActionTriggerBox); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyActionTriggerBox(AMyActionTriggerBox&&); \
	NO_API AMyActionTriggerBox(const AMyActionTriggerBox&); \
public:


#define Simple_stealth_game_Source_Simple_stealth_game_Public_MyActionTriggerBox_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyActionTriggerBox(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyActionTriggerBox(AMyActionTriggerBox&&); \
	NO_API AMyActionTriggerBox(const AMyActionTriggerBox&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyActionTriggerBox); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyActionTriggerBox); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyActionTriggerBox)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_MyActionTriggerBox_h_17_PRIVATE_PROPERTY_OFFSET
#define Simple_stealth_game_Source_Simple_stealth_game_Public_MyActionTriggerBox_h_14_PROLOG
#define Simple_stealth_game_Source_Simple_stealth_game_Public_MyActionTriggerBox_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MyActionTriggerBox_h_17_PRIVATE_PROPERTY_OFFSET \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MyActionTriggerBox_h_17_SPARSE_DATA \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MyActionTriggerBox_h_17_RPC_WRAPPERS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MyActionTriggerBox_h_17_INCLASS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MyActionTriggerBox_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Simple_stealth_game_Source_Simple_stealth_game_Public_MyActionTriggerBox_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MyActionTriggerBox_h_17_PRIVATE_PROPERTY_OFFSET \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MyActionTriggerBox_h_17_SPARSE_DATA \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MyActionTriggerBox_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MyActionTriggerBox_h_17_INCLASS_NO_PURE_DECLS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MyActionTriggerBox_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SIMPLE_STEALTH_GAME_API UClass* StaticClass<class AMyActionTriggerBox>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Simple_stealth_game_Source_Simple_stealth_game_Public_MyActionTriggerBox_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
