// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Simple_stealth_game/Public/MyEnDisableActorsTriggerBox.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyEnDisableActorsTriggerBox() {}
// Cross Module References
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_AMyEnDisableActorsTriggerBox_NoRegister();
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_AMyEnDisableActorsTriggerBox();
	ENGINE_API UClass* Z_Construct_UClass_ATriggerBox();
	UPackage* Z_Construct_UPackage__Script_Simple_stealth_game();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	void AMyEnDisableActorsTriggerBox::StaticRegisterNativesAMyEnDisableActorsTriggerBox()
	{
	}
	UClass* Z_Construct_UClass_AMyEnDisableActorsTriggerBox_NoRegister()
	{
		return AMyEnDisableActorsTriggerBox::StaticClass();
	}
	struct Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnInput_DisActors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OnInput_DisActors;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OnInput_DisActors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnInput_EnActors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OnInput_EnActors;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OnInput_EnActors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnActors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_EnActors;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EnActors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisActors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DisActors;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DisActors_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ATriggerBox,
		(UObject* (*)())Z_Construct_UPackage__Script_Simple_stealth_game,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "MyEnDisableActorsTriggerBox.h" },
		{ "ModuleRelativePath", "Public/MyEnDisableActorsTriggerBox.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_OnInput_DisActors_MetaData[] = {
		{ "Category", "MyEnDisableActorsTriggerBox" },
		{ "ModuleRelativePath", "Public/MyEnDisableActorsTriggerBox.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_OnInput_DisActors = { "OnInput_DisActors", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMyEnDisableActorsTriggerBox, OnInput_DisActors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_OnInput_DisActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_OnInput_DisActors_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_OnInput_DisActors_Inner = { "OnInput_DisActors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_OnInput_EnActors_MetaData[] = {
		{ "Category", "MyEnDisableActorsTriggerBox" },
		{ "ModuleRelativePath", "Public/MyEnDisableActorsTriggerBox.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_OnInput_EnActors = { "OnInput_EnActors", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMyEnDisableActorsTriggerBox, OnInput_EnActors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_OnInput_EnActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_OnInput_EnActors_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_OnInput_EnActors_Inner = { "OnInput_EnActors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_EnActors_MetaData[] = {
		{ "Category", "MyEnDisableActorsTriggerBox" },
		{ "ModuleRelativePath", "Public/MyEnDisableActorsTriggerBox.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_EnActors = { "EnActors", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMyEnDisableActorsTriggerBox, EnActors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_EnActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_EnActors_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_EnActors_Inner = { "EnActors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_DisActors_MetaData[] = {
		{ "Category", "MyEnDisableActorsTriggerBox" },
		{ "ModuleRelativePath", "Public/MyEnDisableActorsTriggerBox.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_DisActors = { "DisActors", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMyEnDisableActorsTriggerBox, DisActors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_DisActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_DisActors_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_DisActors_Inner = { "DisActors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_OnInput_DisActors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_OnInput_DisActors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_OnInput_EnActors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_OnInput_EnActors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_EnActors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_EnActors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_DisActors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::NewProp_DisActors_Inner,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMyEnDisableActorsTriggerBox>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::ClassParams = {
		&AMyEnDisableActorsTriggerBox::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMyEnDisableActorsTriggerBox()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMyEnDisableActorsTriggerBox, 2324888266);
	template<> SIMPLE_STEALTH_GAME_API UClass* StaticClass<AMyEnDisableActorsTriggerBox>()
	{
		return AMyEnDisableActorsTriggerBox::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMyEnDisableActorsTriggerBox(Z_Construct_UClass_AMyEnDisableActorsTriggerBox, &AMyEnDisableActorsTriggerBox::StaticClass, TEXT("/Script/Simple_stealth_game"), TEXT("AMyEnDisableActorsTriggerBox"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMyEnDisableActorsTriggerBox);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
