// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Simple_stealth_game/Public/FPS_GameOver.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFPS_GameOver() {}
// Cross Module References
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_UFPS_GameOver_NoRegister();
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_UFPS_GameOver();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	UPackage* Z_Construct_UPackage__Script_Simple_stealth_game();
// End Cross Module References
	void UFPS_GameOver::StaticRegisterNativesUFPS_GameOver()
	{
	}
	UClass* Z_Construct_UClass_UFPS_GameOver_NoRegister()
	{
		return UFPS_GameOver::StaticClass();
	}
	struct Z_Construct_UClass_UFPS_GameOver_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFPS_GameOver_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_Simple_stealth_game,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFPS_GameOver_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "FPS_GameOver.h" },
		{ "ModuleRelativePath", "Public/FPS_GameOver.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFPS_GameOver_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFPS_GameOver>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFPS_GameOver_Statics::ClassParams = {
		&UFPS_GameOver::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFPS_GameOver_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFPS_GameOver_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFPS_GameOver()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFPS_GameOver_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFPS_GameOver, 195187893);
	template<> SIMPLE_STEALTH_GAME_API UClass* StaticClass<UFPS_GameOver>()
	{
		return UFPS_GameOver::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFPS_GameOver(Z_Construct_UClass_UFPS_GameOver, &UFPS_GameOver::StaticClass, TEXT("/Script/Simple_stealth_game"), TEXT("UFPS_GameOver"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFPS_GameOver);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
