// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Simple_stealth_game/Public/MyEnDisMatEmissiveEventActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyEnDisMatEmissiveEventActor() {}
// Cross Module References
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_NoRegister();
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_AMyEnDisMatEmissiveEventActor();
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_AMyEventActor();
	UPackage* Z_Construct_UPackage__Script_Simple_stealth_game();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	void AMyEnDisMatEmissiveEventActor::StaticRegisterNativesAMyEnDisMatEmissiveEventActor()
	{
	}
	UClass* Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_NoRegister()
	{
		return AMyEnDisMatEmissiveEventActor::StaticClass();
	}
	struct Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetActors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TargetActors;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TargetActors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_MeshIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SetEmissEnable_MetaData[];
#endif
		static void NewProp_SetEmissEnable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_SetEmissEnable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AMyEventActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Simple_stealth_game,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "MyEnDisMatEmissiveEventActor.h" },
		{ "ModuleRelativePath", "Public/MyEnDisMatEmissiveEventActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::NewProp_TargetActors_MetaData[] = {
		{ "Category", "MyEnDisMatEmissiveEventActor" },
		{ "ModuleRelativePath", "Public/MyEnDisMatEmissiveEventActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::NewProp_TargetActors = { "TargetActors", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMyEnDisMatEmissiveEventActor, TargetActors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::NewProp_TargetActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::NewProp_TargetActors_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::NewProp_TargetActors_Inner = { "TargetActors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::NewProp_MeshIndex_MetaData[] = {
		{ "Category", "MyEnDisMatEmissiveEventActor" },
		{ "ModuleRelativePath", "Public/MyEnDisMatEmissiveEventActor.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::NewProp_MeshIndex = { "MeshIndex", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMyEnDisMatEmissiveEventActor, MeshIndex), METADATA_PARAMS(Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::NewProp_MeshIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::NewProp_MeshIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::NewProp_SetEmissEnable_MetaData[] = {
		{ "Category", "MyEnDisMatEmissiveEventActor" },
		{ "ModuleRelativePath", "Public/MyEnDisMatEmissiveEventActor.h" },
	};
#endif
	void Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::NewProp_SetEmissEnable_SetBit(void* Obj)
	{
		((AMyEnDisMatEmissiveEventActor*)Obj)->SetEmissEnable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::NewProp_SetEmissEnable = { "SetEmissEnable", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AMyEnDisMatEmissiveEventActor), &Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::NewProp_SetEmissEnable_SetBit, METADATA_PARAMS(Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::NewProp_SetEmissEnable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::NewProp_SetEmissEnable_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::NewProp_TargetActors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::NewProp_TargetActors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::NewProp_MeshIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::NewProp_SetEmissEnable,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMyEnDisMatEmissiveEventActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::ClassParams = {
		&AMyEnDisMatEmissiveEventActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMyEnDisMatEmissiveEventActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMyEnDisMatEmissiveEventActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMyEnDisMatEmissiveEventActor, 150765333);
	template<> SIMPLE_STEALTH_GAME_API UClass* StaticClass<AMyEnDisMatEmissiveEventActor>()
	{
		return AMyEnDisMatEmissiveEventActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMyEnDisMatEmissiveEventActor(Z_Construct_UClass_AMyEnDisMatEmissiveEventActor, &AMyEnDisMatEmissiveEventActor::StaticClass, TEXT("/Script/Simple_stealth_game"), TEXT("AMyEnDisMatEmissiveEventActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMyEnDisMatEmissiveEventActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
