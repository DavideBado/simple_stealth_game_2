// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SIMPLE_STEALTH_GAME_FP_FirstPersonCharacter_generated_h
#error "FP_FirstPersonCharacter.generated.h already included, missing '#pragma once' in FP_FirstPersonCharacter.h"
#endif
#define SIMPLE_STEALTH_GAME_FP_FirstPersonCharacter_generated_h

#define Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonCharacter_h_17_DELEGATE \
static inline void FUIDelegate_DelegateWrapper(const FMulticastScriptDelegate& UIDelegate) \
{ \
	UIDelegate.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonCharacter_h_22_SPARSE_DATA
#define Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonCharacter_h_22_RPC_WRAPPERS
#define Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonCharacter_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonCharacter_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFP_FirstPersonCharacter(); \
	friend struct Z_Construct_UClass_AFP_FirstPersonCharacter_Statics; \
public: \
	DECLARE_CLASS(AFP_FirstPersonCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Simple_stealth_game"), NO_API) \
	DECLARE_SERIALIZER(AFP_FirstPersonCharacter)


#define Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonCharacter_h_22_INCLASS \
private: \
	static void StaticRegisterNativesAFP_FirstPersonCharacter(); \
	friend struct Z_Construct_UClass_AFP_FirstPersonCharacter_Statics; \
public: \
	DECLARE_CLASS(AFP_FirstPersonCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Simple_stealth_game"), NO_API) \
	DECLARE_SERIALIZER(AFP_FirstPersonCharacter)


#define Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonCharacter_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFP_FirstPersonCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFP_FirstPersonCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFP_FirstPersonCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFP_FirstPersonCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFP_FirstPersonCharacter(AFP_FirstPersonCharacter&&); \
	NO_API AFP_FirstPersonCharacter(const AFP_FirstPersonCharacter&); \
public:


#define Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonCharacter_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFP_FirstPersonCharacter(AFP_FirstPersonCharacter&&); \
	NO_API AFP_FirstPersonCharacter(const AFP_FirstPersonCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFP_FirstPersonCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFP_FirstPersonCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFP_FirstPersonCharacter)


#define Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonCharacter_h_22_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(AFP_FirstPersonCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(AFP_FirstPersonCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(AFP_FirstPersonCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__ParticleSystemComponent() { return STRUCT_OFFSET(AFP_FirstPersonCharacter, ParticleSystemComponent); } \
	FORCEINLINE static uint32 __PPO__FullEnergyParticle() { return STRUCT_OFFSET(AFP_FirstPersonCharacter, FullEnergyParticle); } \
	FORCEINLINE static uint32 __PPO__LoadGameOverUI() { return STRUCT_OFFSET(AFP_FirstPersonCharacter, LoadGameOverUI); } \
	FORCEINLINE static uint32 __PPO__EnergyBullet() { return STRUCT_OFFSET(AFP_FirstPersonCharacter, EnergyBullet); } \
	FORCEINLINE static uint32 __PPO__Bullets_pool() { return STRUCT_OFFSET(AFP_FirstPersonCharacter, Bullets_pool); } \
	FORCEINLINE static uint32 __PPO__GrabbedObject() { return STRUCT_OFFSET(AFP_FirstPersonCharacter, GrabbedObject); } \
	FORCEINLINE static uint32 __PPO__GrabbedLaser() { return STRUCT_OFFSET(AFP_FirstPersonCharacter, GrabbedLaser); }


#define Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonCharacter_h_19_PROLOG
#define Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonCharacter_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonCharacter_h_22_PRIVATE_PROPERTY_OFFSET \
	Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonCharacter_h_22_SPARSE_DATA \
	Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonCharacter_h_22_RPC_WRAPPERS \
	Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonCharacter_h_22_INCLASS \
	Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonCharacter_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonCharacter_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonCharacter_h_22_PRIVATE_PROPERTY_OFFSET \
	Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonCharacter_h_22_SPARSE_DATA \
	Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonCharacter_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonCharacter_h_22_INCLASS_NO_PURE_DECLS \
	Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonCharacter_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SIMPLE_STEALTH_GAME_API UClass* StaticClass<class AFP_FirstPersonCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Simple_stealth_game_Source_Simple_stealth_game_FP_FirstPerson_FP_FirstPersonCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
