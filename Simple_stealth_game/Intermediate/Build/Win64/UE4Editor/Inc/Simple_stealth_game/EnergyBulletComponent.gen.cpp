// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Simple_stealth_game/Public/EnergyBulletComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEnergyBulletComponent() {}
// Cross Module References
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_UEnergyBulletComponent_NoRegister();
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_UEnergyBulletComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_Simple_stealth_game();
	ENGINE_API UClass* Z_Construct_UClass_UParticleSystem_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UParticleSystemComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UEnergyBulletComponent::execKill_enemy)
	{
		P_GET_OBJECT(AActor,Z_Param_enemy);
		P_GET_OBJECT(UParticleSystemComponent,Z_Param_particleComponent);
		P_GET_OBJECT(UParticleSystem,Z_Param_particle);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Kill_enemy(Z_Param_enemy,Z_Param_particleComponent,Z_Param_particle);
		P_NATIVE_END;
	}
	void UEnergyBulletComponent::StaticRegisterNativesUEnergyBulletComponent()
	{
		UClass* Class = UEnergyBulletComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Kill_enemy", &UEnergyBulletComponent::execKill_enemy },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UEnergyBulletComponent_Kill_enemy_Statics
	{
		struct EnergyBulletComponent_eventKill_enemy_Parms
		{
			AActor* enemy;
			UParticleSystemComponent* particleComponent;
			UParticleSystem* particle;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_particle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_particleComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_particleComponent;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_enemy;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEnergyBulletComponent_Kill_enemy_Statics::NewProp_particle = { "particle", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EnergyBulletComponent_eventKill_enemy_Parms, particle), Z_Construct_UClass_UParticleSystem_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnergyBulletComponent_Kill_enemy_Statics::NewProp_particleComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEnergyBulletComponent_Kill_enemy_Statics::NewProp_particleComponent = { "particleComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EnergyBulletComponent_eventKill_enemy_Parms, particleComponent), Z_Construct_UClass_UParticleSystemComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UEnergyBulletComponent_Kill_enemy_Statics::NewProp_particleComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnergyBulletComponent_Kill_enemy_Statics::NewProp_particleComponent_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UEnergyBulletComponent_Kill_enemy_Statics::NewProp_enemy = { "enemy", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EnergyBulletComponent_eventKill_enemy_Parms, enemy), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UEnergyBulletComponent_Kill_enemy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnergyBulletComponent_Kill_enemy_Statics::NewProp_particle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnergyBulletComponent_Kill_enemy_Statics::NewProp_particleComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UEnergyBulletComponent_Kill_enemy_Statics::NewProp_enemy,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UEnergyBulletComponent_Kill_enemy_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/EnergyBulletComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UEnergyBulletComponent_Kill_enemy_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UEnergyBulletComponent, nullptr, "Kill_enemy", nullptr, nullptr, sizeof(EnergyBulletComponent_eventKill_enemy_Parms), Z_Construct_UFunction_UEnergyBulletComponent_Kill_enemy_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnergyBulletComponent_Kill_enemy_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UEnergyBulletComponent_Kill_enemy_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UEnergyBulletComponent_Kill_enemy_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UEnergyBulletComponent_Kill_enemy()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UEnergyBulletComponent_Kill_enemy_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UEnergyBulletComponent_NoRegister()
	{
		return UEnergyBulletComponent::StaticClass();
	}
	struct Z_Construct_UClass_UEnergyBulletComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_min_scale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_min_scale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_max_scale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_max_scale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEnergyBulletComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_Simple_stealth_game,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UEnergyBulletComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UEnergyBulletComponent_Kill_enemy, "Kill_enemy" }, // 1898888774
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnergyBulletComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "EnergyBulletComponent.h" },
		{ "ModuleRelativePath", "Public/EnergyBulletComponent.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnergyBulletComponent_Statics::NewProp_min_scale_MetaData[] = {
		{ "Category", "EnergyBulletComponent" },
		{ "ModuleRelativePath", "Public/EnergyBulletComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UEnergyBulletComponent_Statics::NewProp_min_scale = { "min_scale", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEnergyBulletComponent, min_scale), METADATA_PARAMS(Z_Construct_UClass_UEnergyBulletComponent_Statics::NewProp_min_scale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEnergyBulletComponent_Statics::NewProp_min_scale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnergyBulletComponent_Statics::NewProp_max_scale_MetaData[] = {
		{ "Category", "EnergyBulletComponent" },
		{ "ModuleRelativePath", "Public/EnergyBulletComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UEnergyBulletComponent_Statics::NewProp_max_scale = { "max_scale", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEnergyBulletComponent, max_scale), METADATA_PARAMS(Z_Construct_UClass_UEnergyBulletComponent_Statics::NewProp_max_scale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEnergyBulletComponent_Statics::NewProp_max_scale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEnergyBulletComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnergyBulletComponent_Statics::NewProp_min_scale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnergyBulletComponent_Statics::NewProp_max_scale,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEnergyBulletComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEnergyBulletComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEnergyBulletComponent_Statics::ClassParams = {
		&UEnergyBulletComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UEnergyBulletComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UEnergyBulletComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UEnergyBulletComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEnergyBulletComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEnergyBulletComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEnergyBulletComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEnergyBulletComponent, 1524066496);
	template<> SIMPLE_STEALTH_GAME_API UClass* StaticClass<UEnergyBulletComponent>()
	{
		return UEnergyBulletComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEnergyBulletComponent(Z_Construct_UClass_UEnergyBulletComponent, &UEnergyBulletComponent::StaticClass, TEXT("/Script/Simple_stealth_game"), TEXT("UEnergyBulletComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEnergyBulletComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
