// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SIMPLE_STEALTH_GAME_MyEnDisableActorsTriggerBox_generated_h
#error "MyEnDisableActorsTriggerBox.generated.h already included, missing '#pragma once' in MyEnDisableActorsTriggerBox.h"
#endif
#define SIMPLE_STEALTH_GAME_MyEnDisableActorsTriggerBox_generated_h

#define Simple_stealth_game_Source_Simple_stealth_game_Public_MyEnDisableActorsTriggerBox_h_16_SPARSE_DATA
#define Simple_stealth_game_Source_Simple_stealth_game_Public_MyEnDisableActorsTriggerBox_h_16_RPC_WRAPPERS
#define Simple_stealth_game_Source_Simple_stealth_game_Public_MyEnDisableActorsTriggerBox_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Simple_stealth_game_Source_Simple_stealth_game_Public_MyEnDisableActorsTriggerBox_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyEnDisableActorsTriggerBox(); \
	friend struct Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics; \
public: \
	DECLARE_CLASS(AMyEnDisableActorsTriggerBox, ATriggerBox, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Simple_stealth_game"), NO_API) \
	DECLARE_SERIALIZER(AMyEnDisableActorsTriggerBox)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_MyEnDisableActorsTriggerBox_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAMyEnDisableActorsTriggerBox(); \
	friend struct Z_Construct_UClass_AMyEnDisableActorsTriggerBox_Statics; \
public: \
	DECLARE_CLASS(AMyEnDisableActorsTriggerBox, ATriggerBox, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Simple_stealth_game"), NO_API) \
	DECLARE_SERIALIZER(AMyEnDisableActorsTriggerBox)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_MyEnDisableActorsTriggerBox_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyEnDisableActorsTriggerBox(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyEnDisableActorsTriggerBox) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyEnDisableActorsTriggerBox); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyEnDisableActorsTriggerBox); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyEnDisableActorsTriggerBox(AMyEnDisableActorsTriggerBox&&); \
	NO_API AMyEnDisableActorsTriggerBox(const AMyEnDisableActorsTriggerBox&); \
public:


#define Simple_stealth_game_Source_Simple_stealth_game_Public_MyEnDisableActorsTriggerBox_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyEnDisableActorsTriggerBox(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyEnDisableActorsTriggerBox(AMyEnDisableActorsTriggerBox&&); \
	NO_API AMyEnDisableActorsTriggerBox(const AMyEnDisableActorsTriggerBox&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyEnDisableActorsTriggerBox); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyEnDisableActorsTriggerBox); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyEnDisableActorsTriggerBox)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_MyEnDisableActorsTriggerBox_h_16_PRIVATE_PROPERTY_OFFSET
#define Simple_stealth_game_Source_Simple_stealth_game_Public_MyEnDisableActorsTriggerBox_h_13_PROLOG
#define Simple_stealth_game_Source_Simple_stealth_game_Public_MyEnDisableActorsTriggerBox_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MyEnDisableActorsTriggerBox_h_16_PRIVATE_PROPERTY_OFFSET \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MyEnDisableActorsTriggerBox_h_16_SPARSE_DATA \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MyEnDisableActorsTriggerBox_h_16_RPC_WRAPPERS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MyEnDisableActorsTriggerBox_h_16_INCLASS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MyEnDisableActorsTriggerBox_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Simple_stealth_game_Source_Simple_stealth_game_Public_MyEnDisableActorsTriggerBox_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MyEnDisableActorsTriggerBox_h_16_PRIVATE_PROPERTY_OFFSET \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MyEnDisableActorsTriggerBox_h_16_SPARSE_DATA \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MyEnDisableActorsTriggerBox_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MyEnDisableActorsTriggerBox_h_16_INCLASS_NO_PURE_DECLS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_MyEnDisableActorsTriggerBox_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SIMPLE_STEALTH_GAME_API UClass* StaticClass<class AMyEnDisableActorsTriggerBox>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Simple_stealth_game_Source_Simple_stealth_game_Public_MyEnDisableActorsTriggerBox_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
