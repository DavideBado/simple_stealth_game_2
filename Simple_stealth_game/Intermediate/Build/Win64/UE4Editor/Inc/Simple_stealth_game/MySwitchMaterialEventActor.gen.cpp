// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Simple_stealth_game/Public/MySwitchMaterialEventActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMySwitchMaterialEventActor() {}
// Cross Module References
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_AMySwitchMaterialEventActor_NoRegister();
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_AMySwitchMaterialEventActor();
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_AMyEventActor();
	UPackage* Z_Construct_UPackage__Script_Simple_stealth_game();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstance_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	void AMySwitchMaterialEventActor::StaticRegisterNativesAMySwitchMaterialEventActor()
	{
	}
	UClass* Z_Construct_UClass_AMySwitchMaterialEventActor_NoRegister()
	{
		return AMySwitchMaterialEventActor::StaticClass();
	}
	struct Z_Construct_UClass_AMySwitchMaterialEventActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Switch_Mat_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Switch_Mat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_MeshIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetActors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TargetActors;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TargetActors_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AMyEventActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Simple_stealth_game,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "MySwitchMaterialEventActor.h" },
		{ "ModuleRelativePath", "Public/MySwitchMaterialEventActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::NewProp_Switch_Mat_MetaData[] = {
		{ "Category", "MySwitchMaterialEventActor" },
		{ "ModuleRelativePath", "Public/MySwitchMaterialEventActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::NewProp_Switch_Mat = { "Switch_Mat", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMySwitchMaterialEventActor, Switch_Mat), Z_Construct_UClass_UMaterialInstance_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::NewProp_Switch_Mat_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::NewProp_Switch_Mat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::NewProp_MeshIndex_MetaData[] = {
		{ "Category", "MySwitchMaterialEventActor" },
		{ "ModuleRelativePath", "Public/MySwitchMaterialEventActor.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::NewProp_MeshIndex = { "MeshIndex", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMySwitchMaterialEventActor, MeshIndex), METADATA_PARAMS(Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::NewProp_MeshIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::NewProp_MeshIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::NewProp_TargetActors_MetaData[] = {
		{ "Category", "MySwitchMaterialEventActor" },
		{ "ModuleRelativePath", "Public/MySwitchMaterialEventActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::NewProp_TargetActors = { "TargetActors", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMySwitchMaterialEventActor, TargetActors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::NewProp_TargetActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::NewProp_TargetActors_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::NewProp_TargetActors_Inner = { "TargetActors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::NewProp_Switch_Mat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::NewProp_MeshIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::NewProp_TargetActors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::NewProp_TargetActors_Inner,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMySwitchMaterialEventActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::ClassParams = {
		&AMySwitchMaterialEventActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMySwitchMaterialEventActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMySwitchMaterialEventActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMySwitchMaterialEventActor, 2173686408);
	template<> SIMPLE_STEALTH_GAME_API UClass* StaticClass<AMySwitchMaterialEventActor>()
	{
		return AMySwitchMaterialEventActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMySwitchMaterialEventActor(Z_Construct_UClass_AMySwitchMaterialEventActor, &AMySwitchMaterialEventActor::StaticClass, TEXT("/Script/Simple_stealth_game"), TEXT("AMySwitchMaterialEventActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMySwitchMaterialEventActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
