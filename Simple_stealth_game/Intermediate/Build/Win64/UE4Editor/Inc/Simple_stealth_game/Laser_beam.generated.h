// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SIMPLE_STEALTH_GAME_Laser_beam_generated_h
#error "Laser_beam.generated.h already included, missing '#pragma once' in Laser_beam.h"
#endif
#define SIMPLE_STEALTH_GAME_Laser_beam_generated_h

#define Simple_stealth_game_Source_Simple_stealth_game_Public_Laser_beam_h_12_SPARSE_DATA
#define Simple_stealth_game_Source_Simple_stealth_game_Public_Laser_beam_h_12_RPC_WRAPPERS
#define Simple_stealth_game_Source_Simple_stealth_game_Public_Laser_beam_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Simple_stealth_game_Source_Simple_stealth_game_Public_Laser_beam_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesALaser_beam(); \
	friend struct Z_Construct_UClass_ALaser_beam_Statics; \
public: \
	DECLARE_CLASS(ALaser_beam, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Simple_stealth_game"), NO_API) \
	DECLARE_SERIALIZER(ALaser_beam)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_Laser_beam_h_12_INCLASS \
private: \
	static void StaticRegisterNativesALaser_beam(); \
	friend struct Z_Construct_UClass_ALaser_beam_Statics; \
public: \
	DECLARE_CLASS(ALaser_beam, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Simple_stealth_game"), NO_API) \
	DECLARE_SERIALIZER(ALaser_beam)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_Laser_beam_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ALaser_beam(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ALaser_beam) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALaser_beam); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALaser_beam); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALaser_beam(ALaser_beam&&); \
	NO_API ALaser_beam(const ALaser_beam&); \
public:


#define Simple_stealth_game_Source_Simple_stealth_game_Public_Laser_beam_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ALaser_beam(ALaser_beam&&); \
	NO_API ALaser_beam(const ALaser_beam&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ALaser_beam); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ALaser_beam); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ALaser_beam)


#define Simple_stealth_game_Source_Simple_stealth_game_Public_Laser_beam_h_12_PRIVATE_PROPERTY_OFFSET
#define Simple_stealth_game_Source_Simple_stealth_game_Public_Laser_beam_h_9_PROLOG
#define Simple_stealth_game_Source_Simple_stealth_game_Public_Laser_beam_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Simple_stealth_game_Source_Simple_stealth_game_Public_Laser_beam_h_12_PRIVATE_PROPERTY_OFFSET \
	Simple_stealth_game_Source_Simple_stealth_game_Public_Laser_beam_h_12_SPARSE_DATA \
	Simple_stealth_game_Source_Simple_stealth_game_Public_Laser_beam_h_12_RPC_WRAPPERS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_Laser_beam_h_12_INCLASS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_Laser_beam_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Simple_stealth_game_Source_Simple_stealth_game_Public_Laser_beam_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Simple_stealth_game_Source_Simple_stealth_game_Public_Laser_beam_h_12_PRIVATE_PROPERTY_OFFSET \
	Simple_stealth_game_Source_Simple_stealth_game_Public_Laser_beam_h_12_SPARSE_DATA \
	Simple_stealth_game_Source_Simple_stealth_game_Public_Laser_beam_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_Laser_beam_h_12_INCLASS_NO_PURE_DECLS \
	Simple_stealth_game_Source_Simple_stealth_game_Public_Laser_beam_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SIMPLE_STEALTH_GAME_API UClass* StaticClass<class ALaser_beam>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Simple_stealth_game_Source_Simple_stealth_game_Public_Laser_beam_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
