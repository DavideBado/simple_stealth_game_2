// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Simple_stealth_game/Public/LaserActorBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLaserActorBase() {}
// Cross Module References
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_ALaserActorBase_NoRegister();
	SIMPLE_STEALTH_GAME_API UClass* Z_Construct_UClass_ALaserActorBase();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Simple_stealth_game();
// End Cross Module References
	void ALaserActorBase::StaticRegisterNativesALaserActorBase()
	{
	}
	UClass* Z_Construct_UClass_ALaserActorBase_NoRegister()
	{
		return ALaserActorBase::StaticClass();
	}
	struct Z_Construct_UClass_ALaserActorBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ALaserActorBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Simple_stealth_game,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ALaserActorBase_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "LaserActorBase.h" },
		{ "ModuleRelativePath", "Public/LaserActorBase.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ALaserActorBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ALaserActorBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ALaserActorBase_Statics::ClassParams = {
		&ALaserActorBase::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ALaserActorBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ALaserActorBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ALaserActorBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ALaserActorBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ALaserActorBase, 396896451);
	template<> SIMPLE_STEALTH_GAME_API UClass* StaticClass<ALaserActorBase>()
	{
		return ALaserActorBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ALaserActorBase(Z_Construct_UClass_ALaserActorBase, &ALaserActorBase::StaticClass, TEXT("/Script/Simple_stealth_game"), TEXT("ALaserActorBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ALaserActorBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
